---
title: "About Me"
date: 2018-12-03T16:33:14+02:00
tags:
categories:
draft: false
---

I am, or rather, my online nickname is Samsai. I am a Finnish Linux user, student and
a tech enthusiast. Currently I am working on my Bachelor's Degree in Computer Science in 
the University of Helsinki.

I am part of the [GamingOnLinux.com](http://www.gamingonlinux.com) editorial team and
I stream Linux games on a regular basis on [Twitch](http://www.twitch.tv/sirsamsai).

Occasionally I also write software. Here are some of the projects I've worked on:

- [GOLBot IRC Bot](https://gitlab.com/Samsai/golbot-rust)
- [ENDUSER](https://samsai.itch.io/end-user)
- [Cursed Pl@tformer](https://samsai.itch.io/cursed-platformer)
- [Rust Roguelike](https://gitlab.com/Samsai/rust-roguelike)

# About this website

This website acts as my creative outlet for my more rambly thoughts. My writing
will most likely be centered around Linux, gaming and programming, although
I won't promise I will stay entirely within just those domains.
