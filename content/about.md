---
title: "About Me"
draft: false
---

I am, or rather, my online nickname is Samsai. I am a Finnish Linux user, tech enthusiast
of the kind that doesn't own a printer. I have a Master's Degree in Computer Science and
work in a big telecommunications company.

I am part of the [GamingOnLinux.com](http://www.gamingonlinux.com) editorial team and
I stream Linux games on a regular basis on [Owncast](https://cast.samsai.eu).

## You can find me here

- Email: samsai@posteo.net
- Mastodon: https://mastodon.social/@Samsai
- Owncast: https://cast.samsai.eu/

## My projects

Occasionally I also write software. Here are some of the projects I've worked on:

### Games
- [Incoming](https://samsai.itch.io/incoming)
- [Robot Richochet](https://codeberg.org/Samsai/mirrored-lasers)
- [Cursed Pl@tformer](https://samsai.itch.io/cursed-platformer)
- [Rust Roguelike](https://gitlab.com/Samsai/rust-roguelike)
- [ENDUSER](https://samsai.itch.io/end-user)
- [Collateral Damage](https://samsai.itch.io/collateral-damage)
- [BEAT NOID](https://samsai.itch.io/beat-noid)
- [Alpha Orbit](https://samsai.itch.io/alpha-orbit)

### Web stuff
- [EcoBanger](https://eco.samsai.eu)
- [Qud Creator](https://codeberg.org/Samsai/qud-creator)
- [Owncast Chat Overlay](https://codeberg.org/Samsai/owncast-chat-overlay)

### Utilities
- [GOLBot IRC Bot](https://codeberg.org/Samsai/golbot-rust)

# About this website

This website acts as my creative outlet for my more rambly thoughts. My writing
will most likely be centered around Linux, gaming and programming, although
I won't promise I will stay entirely within just those domains.
