---
title: "We'll always have Quake"
date: 2019-06-28T16:58:13+03:00
tags:
  - gaming
---

I've noticed a pattern in video games where the lifespan of many video games is being cut rather short. And I
don't mean that in the sense that the developers are spending less time on each game, in fact due to wide online
connectivity games probably get a lot more attention from the developers, since it's easy for them to patch and
update games after launch.

What I mean is that you can buy a game today and in a few years that game might end up not being available to
you anymore in a usable state. Usually this happens because many modern games are tightly coupled to certain
online systems, such as developer-run servers which the developers will take offline after keeping those systems
running is no longer financially viable. I naturally understand that you cannot force games companies to keep
hosting the servers beyond that point but the customer practically ends up losing access to the product at that
point regardless.

Companies are also interested in the concept of game streaming and recently Google decided to announce their own
streaming platform in the form of Stadia. The basic gist of it is that the games are run on remote server systems
and the video output is streamed to your terminal device and your input is sent back to the server. For developers
this has a number of benefits. For example, developers need to only target a single system configuration that will
remain essentially immutable, similar to targeting a console, which reduces support costs and allows for specific
optimizations. Secondly, streaming is the ultimate form of DRM, since not a single piece of the game will be running
on the user's system.

Sadly, this also has the effect of potentially locking yourself out of your game library for good if or when the
platform goes down (luckily that never happens with Google's products /sarcasm). This also means that games that
might end up getting pulled from the store, either due to a time-limited deal or because of other licensing concerns,
so you may end up losing access even if the platform itself continued working as normal.

However, not depending on some DRM or game servers doesn't necessarily guarantee a game's longevity either.
Recently the Linux community experienced some turmoil due to Ubuntu announcing that they would drop 32-bit packages
from Ubuntu 19.10 onwards. Whether or not that actually goes through after all of the backlash is another question
entirely, but it does highlight an issue with legacy, namely that maintaining compatibility with it comes with a
cost that some distributions might not be willing to pay. Also, if we look into the past, the ports released by
Loki at the turn of the millenium have also degraded, even when 32-bit libraries were being built. Technology moves
forward and eventually decisions will need to be made where to draw a line, or sacrifice your forward momentum
for the sake of maintaining compatibility. These problems would become even worse, should we eventually decide to
move on from our current display servers, graphics APIs or even CPU architecture to something else. And I certainly
hope we wouldn't forever be stuck on x86 simply because that's what we used in the past.

![quake screenshot](/img/quake/quake.jpg)

Luckily there are also games that will not disappear into the void just like that. One example that I can think
of is Quake. Quake is now over 20 years old, but I can quite happily play it right now. In addition to that, there
are also reasonable guarantees that I will be able to play Quake even if another 20 years go by. The reason for this
is that in addition to not having to rely on a centralized matchmaking server or some such system, the game is
also completely open source. As long as somebody cares, they can go and fork the source code to modify it to add
new features, fix bugs or to port the game to a whole new platform. And indeed, that has been done in the past.
When graphics drivers started getting Vulkan support, a version of Quake was crafted that made use of it. And
when SiFive announced their SiFive Unleashed RISC-V board, they demonstrated Quake running on it.

So, I think Quake is an important example to keep in mind when we think about the lifespans of our games. I am
not a free software fanatic and I understand that it's not viable or possible for many game developers (and
software developers in general) to open source their games. For example, with proprietary game engines it wouldn't
necessarily even accomplish much. However, I think there is immense value in free/open source software and I hope
that if a developer happens to stumble upon this post, they'd consider setting their software free at some point
in order to allow the game to reach a relative state of immortality.

As users we should also not forget the value of these open source projects that either maintain older games or make
entirely new games available to us in an open source form. Examples of these kinds of awesome projects are [OpenMW](http://openmw.org/) and [OpenRA](https://www.openra.net/) but there are plenty more out there, not to forget amazing original
open source games like [Xonotic](https://xonotic.org/) and [0 A.D.](https://play0ad.com/) of course.

Let us not leave the games we play be left in the past and be forgotten.
