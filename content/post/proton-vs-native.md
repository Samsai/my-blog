---
title: "Proton and Linux ports and 'no Tux no Bux'"
date: 2019-01-02T14:00:13+02:00
tags:
  - gaming
  - linux
categories:
draft: false
---

***Note***: When I refer to "native Linux gaming" in this post I mean officially supported Linux ports.

I am a very staunch supporter of native Linux gaming and have been for years now. Like other people, in the earlier days of
my Linux gaming experiences I too relied on Wine and even bought some games to play in Wine, but since the explosion of new
releases that arrived to the platform with the launch of Steam for Linux, I grew to rely on Wine less and less and eventually
stopped buying games to play on Wine, since I noticed that people like Feral Interactive, one of the biggest porting houses
that make native ports for our platform, are negatively affected when people buy games before their ports are released.

Native reigned quite strong for a decent while and Wine kind of stepped into the background for a time, since we seemed to
have new interesting games releasing every other day. However, with the introduction of Proton, a fancy-pants Wine fork 
integrated into Steam, masses of Linux gamers have been flocking to buy and play all sorts of new and old Windows titles.

I have been looking at this movement with a certain degree of contempt and have on numerous occasions spoken on the social
media or article comments or Discord servers about why I don't think flocking to Proton as a solution for Linux gaming is 
a good idea. And since I seem to have to do that multiple times, I figured I might as well write down my thoughts into a 
blog post so that I can link back to it to save up on previous bytes of bandwidth.

## Proton, what is it and what it aims to be

Proton itself isn't much different from Wine, in fact it is a fork of Wine with some out-of-tree extensions added.
Probably the biggest internal feature it boasts is DXVK, the DirectX-over-Vulkan compatibility layer that provides support 
for DirectX 10 and 11. Vanilla Wine has been limited to DirectX 9 support over OpenGL for a good while, which not only 
allows only older titles to be played but also performs relatively poorly in many circumstances due to the difficulty
of mapping DirectX 9 over OpenGL.

Valve also integrated Proton into Steam directly, allowing a number of "officially supported" games to be launched with it
through a feature called Steam Play. These are basically just Windows titles on Steam that Valve has deemed to be sufficiently
compatible with Steam Play to let people run them as if they were native games. They also allow users to optionally attempt
running any Windows games with Steam Play, just without the small guarantee of compatibility offered by the whitelist.

By itself this isn't a bad thing and I have on multiple occasions stated that I am not against Proton as a technology. On
the contrary, I think it's very important to have a tool that assists people in their migration from Windows to Linux, and
letting them take at least some of their Windows titles with them is a fantastic feature. However, the devil is in the details
or, more specifically, how Proton is being used.

## Porting house profits

Porting houses like Feral Interactive and Aspyr Media (and to the smaller degree Virtual Programming) are currently the biggest
source of modern AAA titles on Linux. While I personally don't hold much respect for what the AAA is trying to do these days,
there is no arguing that the demand for these games is big and they are mostly made with a level of detail that cannot be
expected from individual indie game studios. What companies like Feral do is they make deals with the AAA publishers to port
their games to platforms like Mac and Linux and they make their profits based on counting Linux and Mac sales of those games.
We don't really know what goes on in those deals and what exactly the AAA publishers get out of the deal, but the important
thing for us Linux gamers is that the porting house is paid for the purchases that specifically are counted as Linux purchases.

![Feral Interactive Logo](/img/feral-2016-logo.png)

So, if you then decide to purchase a Windows game that eventually gets a Linux port, can you guess how much money the porting
house got from you? Zero, zilch, none, null. On an individual purchase this hardly makes a dent, but when you consider that
even the mass market is just a large collection of individual purchases, it eventually adds up. This leads to decreased profits
for these porting houses, which means that when that game eventually releases that just refuses to work in Proton that could
have been ported to Linux, you won't be getting the port. Many people have complained that some of the ports made by the
porting houses aren't necessarily as great or that Proton runs the games just as well, but they forget that when you buy a
Linux port you actually have some guarantee that the game actually works. It might come with caveats, maybe cross-platform
multiplayer doesn't work or that the port carries a 20% performance penalty. But you will know that while the company that
made the port is still in business and they haven't dropped the support for that port, there is someone holding some responsibility
for that port. Someone you can say "hey, this doesn't work on my machine" and they can respond with something other than
"you aren't using our supported platform".

## "But Valve said Proton counts as Linux!"

When Proton released Valve, in their infinite wisdom, said that playing a game in Proton counts as playing the game on Linux.
This lead to a number of people thinking that if they play Windows games through Steam Play they will be supporting Linux gaming
since they are showing game developers that Linux gamers are interested in their products and that therefore a viable Linux
market exists. 

**I question the validity of that stance.**

To me you playing a Windows game on Steam Play does not show that a viable market is interested in the game. Instead what it says
to me is that said market does not need a Linux port to buy the product. You are basically setting them up for a win-win situation
and then hoping they settle for the less optimal option. When you play their game on Steam Play, it shows that you are willing
to buy a product with no official support, which means that the company can completely avoid spending any support costs on you.
If you have an issue they can just sweep it under the rug of "unsupported platform" and get on with their day. Meanwhile you
have no guarantee that the next update of the game will even work on the game and still you have paid the same price for the
game as everyone else.

To put it in other words, if I made a Linux application (that was a paid product) and I saw a bunch of Windows users using the
application through WSL (Windows Subsystem for Linux), would I have a real incentive to spend my own money on porting the 
application to Windows to appease the users that clearly already bought the application but are having to run it through a 
compatibility layer? For this thought experiment to be a bit more equivalent, let's also assume that the Windows users would 
only represent a maximum of 1% of the customer base. I repeat, they already paid for the application, so I don't even have any 
guarantees that I would be getting any more profit out of them.

That to me is what playing on Proton to hope senpai notices you sounds like. I think it's just unquestionably better to hold
onto my money and reward the developers who decide to support me. And I know some of you will argue that I am only one tiny
person and that my money doesn't make a difference. But if you apply that principle ad nauseam you end up in a situation where
nothing you personally do makes a difference, so you may as well go outside and let those of us who want to make a difference at
least try.

## The ecosystem AKA the long game

Individual games and are individual games. Important in the moment, but eventually forgotten. To me the most important aspect
of Linux gaming is the continued existence of Linux gaming. To have that we need to make sure that the ecosystem Linux gaming is
built around remains in good shape. This means stuff like APIs, drivers and tools that make both playing and developing Linux
games possible. And this is where I believe supporting native Linux games is of particular interest.

If we take Feral Interactive as an example again, they have not only made a number of native ports but while doing so they have
made numerous contributions to the GPU drivers and written tools like GameMode which can be used to extract additional power
out of our Linux systems while gaming. These contributions are important and were Feral not in the business of developing Linux
ports we wouldn't have these things. In a similar sense other game developers help strengthen our platform's ecosystem by
using Linux compatible middleware and APIs, reporting bugs in them creating tools along the way that at first help their lives
and then help the lives of those that came after them. The SDL (Simple DirectMedia Layer) project came into being from the
porting efforts of Loki Software in 1998. Now it powers a vast chunk of the Linux games out there and has served as the
foundation for countless gaming projects, both professional and amateur.

![SDL Logo](/img/sdl-logo.png)

So, when I support developers making Linux ports I am also indirectly making them make the ecosystem better. Any bug report the
developers file against an API or a middleware is a small step forward for ther ecosystem. Any tool or a driver improvement makes 
our platform better for those next in line. This is the long game, and it always makes sense to play for the long game rather
than aim for fleeting short-term benefits that come to bite us in the long-term.

## Do you feel bad yet?

Some people who have read this post might feel quite defensive if they have gotten this far. And I feel like I should probably
reassure you that I don't actually think you are a bad person if some of the things I've mentioned in this blog posts have in some
way described you. Even I occasionally start up some games in Wine, although I don't buy new Windows games for that purpose and I 
have yet to use Proton in the slightest. I wrote this blog post to get people to consider their purchasing decisions and the 
possible consequences of those decisions when it comes to the future of Linux gaming. I also don't think that you playing Windows
games you already own is a bad thing at all. You already paid for the game so there isn't any additional harm you could even be
doing by playing it in Proton or Wine. 

So on that front, go nuts. Clear that backlog of Windows games you accumulated over the years and have fun. Just try to keep in 
mind when considering buying new games that some of us have long lists of reasons to prefer the games that have official support 
over those that just happen to run through a compatibility layer. 

And maybe you too should care about those reasons.
