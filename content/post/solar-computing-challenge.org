---
title: "Solar computing challenge"
date: 2023-09-11T19:28:29+03:00
draft: false
---

Over this summer I've been playing around with some solar panels and
one of my latest little toys is a power bank which supports 45W and 65W
USB-C PD charging. This means that it's capable of charging both my
tablet and my laptop unlike the smaller power banks I've had which only
support 5V charging.

The powerbank in question can also be charged with solar, both with
a dedicated solar plug and also via USB-C. It even accepts regular
5V input, so the USB solar panels that I already have two of will
happily charge the battery.

So, now that I can theoretically run my laptop and tablet purely off
of solar, I figured I'd run a little challenge to see how feasible
that actually is. This week seems moderately sunny and autumn isn't
yet far enough to limit the daylight too much, so I will attempt to
only use my own solar power for my computing needs for this week.

I am limiting the challenge to only my personal computing devices, so
my phone, laptop and tablet. My work laptop is counted outside of the
challenge entirely. I am also not counting the three evenings in the
week when I am streaming on my desktop, since I don't want to disrupt
my streaming schedule. But everything else should run off of solar
power.

So, the computing hardware in question consists of:

- Thinkpad T470s, my personal laptop
- Latitude 7285, my tablet
- Nokia X10, my phone

My power production consists of:

- Xtorm 21W USB solar charger
- Exibel 12W USB solar charger
- Goal Zero Nomad 20 20W solar panel

For power storage I have:

- Goal Zero Sherpa 100AC, ~95 Wh power bank
- Two store brand 22 Wh USB power banks.

[[/img/solar-computing/solar-panels-on-balcony.jpg]]

This means I have a theoretical maximum power production of 53 W and a
maximum of ~140 Wh of battery capacity. However, especially that power
production figure is very much theoretical, since I live in an
apartment and the only place I have for my panels is my balcony. The
balcony gets sunlight from around 2 PM to 8 PM at this time of the
year and the panels are mostly behind the balcony glass. The maximum
actual production I've seen is 17 W with two panels hooked to the power
bank simultaneously, with the average production being around 10W. So,
in an optimal case I can produce maybe 60 Wh per day. However, that
number drops rapidly if the weather is cloudy and the weather forecast
doesn't promise entirely cloudless days.

So, to an extent, the challenge is also about conserving power rather
than just relying solar to consistently keep my devices topped up. If
I am wasteful I will run out of power and need to limit the use of my
devices. If I run out of battery on my phone, I will consider the
challenge failed, since I will need my phone to do work.

* Day 1

I topped up my power bank yesterday to 100% and charged my devices. I
was considering going into this challenge at 80% charge on my power
bank, but I chickened out and decided to give myself the best chance
of success. The smaller power banks were left at whatever charge level
they were at before, which means that one of them is nearly depleted
while the other one has around 75% charge.

I ran updates and installed some tools on my laptop, including my
latest Emacs configs in order to be ready to use it as my main
system. I also set it to power saving mode and tuned the screen
brightness as low as is comfortable. I also switched to light mode
with the hope that I can run the screen at lower brightness to
conserve power. After all modern screens shouldn't care too much what
color pixels you are putting on the screen, the backlight should be
the biggest differentiator in power use.

The day has been semi-cloudy and more overcast towards the end of the
day, which cuts into my power production. But the good thing is that I
haven't yet used my devices much and the power bank is fully topped
up.

#+begin_src 
Weather report: Espoo

     \  /       Partly cloudy
   _ /"".-.     19 °C          
     \_(   ).   ↗ 22 km/h      
     /(___(__)  10 km          
                0.0 mm         
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Mon 04 Sep ├───────────────────────┬──────────────────────────────┐
│            Morning           │             Noon      └──────┬──────┘     Evening           │             Night            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│    \  /       Partly cloudy  │               Cloudy         │    \  /       Partly cloudy  │               Overcast       │
│  _ /"".-.     16 °C          │      .--.     18 °C          │  _ /"".-.     +19(17) °C     │      .--.     16 °C          │
│    \_(   ).   ↗ 14-18 km/h   │   .-(    ).   ↗ 18-23 km/h   │    \_(   ).   ↗ 22 km/h      │   .-(    ).   ↗ 17-26 km/h   │
│    /(___(__)  10 km          │  (___.__)__)  10 km          │    /(___(__)  10 km          │  (___.__)__)  10 km          │
│               0.0 mm | 0%    │               0.0 mm | 0%    │               0.0 mm | 0%    │               0.0 mm | 0%    │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
#+end_src

Both my laptop and phone have synchronized at 74% battery (laptop took
most power when I was native-compiling my Emacs config and
packages). I should be able to top them up without sacrificing too
much power budget. Unfortunately tomorrow looks like it's going to be
somewhat rainy and cloudy, so we'll see how much power we can claw back.

YouTube is the biggest concern for me, since I expect video playback
to be particularly power hungry. I have in the past tried to get
hardware video decoding working, but I don't think I've managed to do
it either on my laptop or my tablet. I could probably consider using
my phone for that, but it's not nearly as nice. Maybe I'll just skip
the videos somehow.

I'll set up some dinner for myself and maybe crawl through some blogs
or read some books instead.

** Power status:

- 95 Wh power bank: 100%
- 22 Wh power banks: >75% and <25% (estimated from power indicators)
- Phone: 74%
- Tablet: 100%
- Laptop: 72%

* Day 2

The weather was pretty bad as predicted, mostly cloudy with very light
rains but most importantly very little sunlight. I left my panels
oriented towards the sunset when I left for the morning and
as expected, I was only able to catch some rays towards the evening.

I recharged both my laptop and tablet yesterday evening and that left
the power bank around 56% and I only managed to claw back a few
percent of charge today, so thus far we are mostly running on battery
capacity rather than actually on self-produced solar.

#+begin_src 
Weather report: Espoo

     \  /       Partly cloudy
   _ /"".-.     20 °C          
     \_(   ).   ↗ 28 km/h      
     /(___(__)  10 km          
                0.0 mm         
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Tue 05 Sep ├───────────────────────┬──────────────────────────────┐
│            Morning           │             Noon      └──────┬──────┘     Evening           │             Night            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│  _`/"".-.     Patchy rain po…│    \  /       Partly cloudy  │               Overcast       │     \   /     Clear          │
│   ,\_(   ).   18 °C          │  _ /"".-.     20 °C          │      .--.     15 °C          │      .-.      +12(10) °C     │
│    /(___(__)  ↗ 19-26 km/h   │    \_(   ).   ↗ 24-28 km/h   │   .-(    ).   ↘ 17-23 km/h   │   ― (   ) ―   ↘ 15-25 km/h   │
│      ‘ ‘ ‘ ‘  10 km          │    /(___(__)  10 km          │  (___.__)__)  10 km          │      `-’      10 km          │
│     ‘ ‘ ‘ ‘   0.1 mm | 86%   │               0.0 mm | 0%    │               0.0 mm | 0%    │     /   \     0.0 mm | 0%    │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
#+end_src


YouTube didn't prove to be as big of a problem though. By watching
videos through MPV and running the laptop in power saving mode, I was
able to squeeze a few videos into just around maybe 10% of
battery. So, as long as the weather improves a bit and I can actually
produce in the afternoon I should be able to include some YouTube into
my daily entertainment without worrying about immediately running out
of battery.

I am trying to take it easy on the battery power today though, with
the weather having been pretty poor. Luckiy tomorrow's forecast is
showing much more clear skies and sun, so hopefully I'll be able to
catch back up.

Because of the low-energy diet today, I opted to do some more reading
and playing guitar. Although guitar isn't that easy, since I typically
read the chords and tabs from online. I tried reading them on my phone
but A.) looking downwards like that is a quick way to get neck pain and
B.) Ultimate-Guitar suffers from the enshittification of mobile
experience just as much, if not more than, various other mainstream
sites. The most outrageous thing was that when I relented and
installed the mobile app they wanted, they wanted to charge me money
for features that are freely available on the regular website, such as
auto-scroll and guitar pro tabs. I pity the people who rely on the
mobile Internet experience and apps, because not only do they have to
watch ads all of the time but they are also being swindled by these
companies too.

At least regular old analog books still work without issues. If I
start running out of power, at least I should be able to finish up
Chapterhouse: Dune way faster than expected.

** Power status:

- 95 Wh power bank: 59%
- 22 Wh power banks: >75% and <25% (estimated from power indicators)
- Phone: 69%
- Tablet: 75%
- Laptop: 73%

* Day 3

Yesterday I drained the big power bank down to 19% when I charged up
my laptop and tablet up to full. I said I was going to go easy on the
power, but I did end up watching some YouTube (I needed to catch up on
the latest Drawfee), which did end up putting my laptop around 50%
charge.

However, the good news is that the forecast was correct and it was
cloudless skies basically the entire day. This allowed me to recharge
the smaller power bank that I had drained up to 50% and the big power
bank back up to 60%. So, from this we can determine that on a good
day, I can produce around 49 Wh, a bit short of the 60 Wh optimal
number I suggested earlier. The difference might have been due to me
not being around at home to adjust the panels to a proper alignment
with the sun or me simply being somewhat optimistic as to the number
of useful hours of sunlight I can get.

#+begin_src 
Weather report: Espoo

      \   /     Clear
       .-.      +12(11) °C     
    ― (   ) ―   → 9 km/h       
       `-’      10 km          
      /   \     0.0 mm         
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Wed 06 Sep ├───────────────────────┬──────────────────────────────┐
│            Morning           │             Noon      └──────┬──────┘     Evening           │             Night            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│     \   /     Sunny          │     \   /     Sunny          │     \   /     Sunny          │     \   /     Clear          │
│      .-.      +14(12) °C     │      .-.      16 °C          │      .-.      +15(14) °C     │      .-.      +12(9) °C      │
│   ― (   ) ―   ↘ 19-23 km/h   │   ― (   ) ―   ↘ 18-21 km/h   │   ― (   ) ―   → 13-21 km/h   │   ― (   ) ―   → 9-22 km/h    │
│      `-’      10 km          │      `-’      10 km          │      `-’      10 km          │      `-’      10 km          │
│     /   \     0.0 mm | 0%    │     /   \     0.0 mm | 0%    │     /   \     0.0 mm | 0%    │     /   \     0.0 mm | 0%    │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
#+end_src

I am also still keeping the second small power bank that I have at
around 75% as my emergency reserve. I imagine that should give me at
least a few good charges of my phone and I can pump it into the bigger
power bank as a boost if I decide I need it.

Another good thing was that I spent most of the day away at my
parents' place since my dad had a birthday party. So, I didn't have
any need to use anything beyond my phone for the most of today. That
should tip the balance a bit more in my favour and hopefully I can
catch some more rays tomorrow to get a power surplus.

Speaking of, tomorrow's forecast is looking like a mix of sun and
clouds. The service from where I am getting the ASCII forecasts,
https://wttr.in, suggests entirely clear skies but I am more likely to
believe the local forecast that predicts more clouds. However, even in
the worst case I am hoping to get maybe 50% of what I produced today,
bringing me hopefully close to 75-80% on the big power bank, minus
what I might use to charge my laptop after I am done with it.

I might end up doing mostly the same thing I did yesterday though,
namely turning in an hour earlier than normal. I don't think I
technically need 9 hours of sleep, but I am not complaining about the
sleep I got last night. I guess having less electronic distractions
helps me relax easier. Who would have known?

Anyway, I think I will probably check quickly if there's something
short and fun on YouTube to watch, maybe clear my RSS feeds and then
consider going to bed and reading a little bit. Tomorrow should be a
fairly long day at work, but if things are going the way they've been
going so far, I don't think lack of sleep is going to be a problem
there.

** Power status:

- 95 Wh power bank: 60%
- 22 Wh power banks: >75% and >50% (estimated from power indicators)
- Phone: 63%
- Tablet: 100%
- Laptop: 93%

* Day 4

A long day at work today, which in the context of the challenge worked
out okay because I was mostly on my work laptop. I am trying to make
sure that I don't "cheat" by just using my work laptop for
entertainment, so in between meetings and "real work" I tried to do
only things which I can reasonably call work-related.

So, I did some Emacs configuration and set Sly and Geiser for Common
Lisp and Guile Scheme development respectively. I've recently decided
to learn more Lisp in order to get better at Emacs configuration and
also to attain enlightenment in programming. Getting better at former,
still need some work on the latter.

The weather worked out really well too today. Despite me ending up
using some more battery last night, I managed to bring the power bank
up to 71% from around 33% or so. So, still very much on a surplus of
power. In fact, despite the forecast suggesting some clouds, there
were barely any on the sky until around sunset.

#+begin_src 
Weather report: Espoo

     \  /       Partly cloudy
   _ /"".-.     16 °C          
     \_(   ).   ↗ 13 km/h      
     /(___(__)  10 km          
                0.0 mm         
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Thu 07 Sep ├───────────────────────┬──────────────────────────────┐
│            Morning           │             Noon      └──────┬──────┘     Evening           │             Night            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│     \   /     Sunny          │    \  /       Partly cloudy  │               Overcast       │    \  /       Partly cloudy  │
│      .-.      13 °C          │  _ /"".-.     17 °C          │      .--.     16 °C          │  _ /"".-.     +16(12) °C     │
│   ― (   ) ―   → 9-11 km/h    │    \_(   ).   → 10-12 km/h   │   .-(    ).   ↗ 13-21 km/h   │    \_(   ).   ↗ 13-19 km/h   │
│      `-’      10 km          │    /(___(__)  10 km          │  (___.__)__)  10 km          │    /(___(__)  10 km          │
│     /   \     0.0 mm | 0%    │               0.0 mm | 0%    │               0.0 mm | 0%    │               0.0 mm | 0%    │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
#+end_src

Tomorrow is showing as partly cloudy for most of the day, so still
decent chance to get some power. It's also going to be stream day
tomorrow, which means I'll be allowed to use my desktop from 8 PM
onwards to play some games, so those things should add up to some more
saved up power. Saturday will also apparently involve being out of the
house for some time, so I will mostly just need to worry about having
enough power for the second half of Saturday and all of Sunday, and
Saturday should be quite sunny.

I also have yet to tap into my emergency reserve and I should have
enough power in the other power bank from today to charge my phone up
to full again.

One thing that I think will be interesting to see is comparing my
electricity consumption from last week to this week when I am done,
that should tell me how much power I am spending each week on just
electronic entertainment and whatnot. If that figure is significant, I
might need to at least consider using my laptop and tablet more and
not booting up the desktop for eveything.

I am missing my big monitors though.

** Power status:

- 95 Wh power bank: 71%
- 22 Wh power banks: >75% and >50% (estimated from power indicators)
- Phone: 72%
- Tablet: 100%
- Laptop: 80%

* Days 5 and 6

I didn't get around to updating the post yesterday before the stream
and afterwards I had already forgotten about it. But as a short
summary, yesterday was fairly easy since the time between me getting
from work and starting streaming was quite short. I also managed to
get some power produced, but I don't have the exact figures.

I also decided to use my tablet a little bit yesterday, since it felt
too much like cheating to use the laptop, since I was going to keep it
plugged in during the stream to make sure I can read the chat and
monitor the stream. However, I did charge my laptop before the stream
from the power bank and after also charging my tablet off of it, I
ended up with around 22% by today morning.

The weather turned out to be a bit worse than I had hoped, the
original forecast for clear skies turned into somewhat cloudy and then
mostly overcast towards the evening. In the afternoon I did manage to
charge the power bank up to 47%, but that's as far as it will go. 

#+begin_src 
Weather report: Espoo

     \  /       Partly cloudy
   _ /"".-.     18 °C          
     \_(   ).   ↖ 17 km/h      
     /(___(__)  10 km          
                0.0 mm         
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Sat 09 Sep ├───────────────────────┬──────────────────────────────┐
│            Morning           │             Noon      └──────┬──────┘     Evening           │             Night            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│     \   /     Sunny          │     \   /     Sunny          │    \  /       Partly cloudy  │     \   /     Clear          │
│      .-.      18 °C          │      .-.      19 °C          │  _ /"".-.     +18(17) °C     │      .-.      14 °C          │
│   ― (   ) ―   ↖ 9-10 km/h    │   ― (   ) ―   ↖ 10-11 km/h   │    \_(   ).   ↖ 17 km/h      │   ― (   ) ―   ↖ 6-13 km/h    │
│      `-’      10 km          │      `-’      10 km          │    /(___(__)  10 km          │      `-’      10 km          │
│     /   \     0.0 mm | 0%    │     /   \     0.0 mm | 0%    │               0.0 mm | 0%    │     /   \     0.0 mm | 0%    │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
#+end_src

I also drained the smaller power bank yesterday, so that one only has
what I managed to collect today. I am probably going to dump that into
my bluetooth headphones and also dump the reserve power bank into my
main power bank just to pool the energy together. Although I will
probably charge my phone up in advance just in case.

I will still need to recharge my tablet, since it's down to a pretty
miserable amount of battery after reading some Structure and
Interpretation of Computer Programs (SICP) on it in the morning. Got a
good 100 or so pages into it and although most of the concepts are
familiar to me, I am enjoying it as a Scheme tutorial of sorts. And
it's good to repeat fundamentals every now and then too.

** Power status:

- 95 Wh power bank: 47%
- 22 Wh power banks: >75% and ~25% (estimated from power indicators)
- Phone: 79%
- Tablet: 37%
- Laptop: 95%

* Day 7 and conclusion

Last day of the challenge and just a few hours to go until I can plug
my devices into a wall socket if I so desire. Quite conveniently too,
since I drained the big power bank to empty this morning and the 20%
that I managed to collect also ended up getting drained when I
recharged my tablet after watching some YouTube on it.

#+begin_src 
Weather report: Espoo

     \  /       Partly cloudy
   _ /"".-.     20 °C          
     \_(   ).   ↗ 11 km/h      
     /(___(__)  10 km          
                0.0 mm         
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Sun 10 Sep ├───────────────────────┬──────────────────────────────┐
│            Morning           │             Noon      └──────┬──────┘     Evening           │             Night            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│               Cloudy         │     \   /     Sunny          │     \   /     Sunny          │               Mist           │
│      .--.     18 °C          │      .-.      21 °C          │      .-.      18 °C          │  _ - _ - _ -  16 °C          │
│   .-(    ).   ↑ 15-20 km/h   │   ― (   ) ―   ↑ 16-19 km/h   │   ― (   ) ―   ↗ 10-18 km/h   │   _ - _ - _   ↑ 8-17 km/h    │
│  (___.__)__)  10 km          │      `-’      10 km          │      `-’      10 km          │  _ - _ - _ -  2 km           │
│               0.0 mm | 0%    │     /   \     0.0 mm | 0%    │     /   \     0.0 mm | 0%    │               0.0 mm | 0%    │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
#+end_src

At this point I can call this challenge a success. I feel like it's
fairly likely that if I were to continue the way I have been going so
far, I would probably run out of power at some point towards the end
of next week, but I could probably reduce my power usage further and
keep going a bit longer if I so desired. If it was still summer and
the weather was consistently sunny, I think I could go for a good
while even with relatively normal computer usage, albeit limited to my
laptop and tablet.

I think the challenge was also overall worthwhile. It gave me a better
idea of how much I could reasonably expect to do with the small solar
setup I have, and honestly it went quite a bit further than I
expected. I was prepared for multiple days of basically not using my
laptop or tablet at all, but basically every day I was able to start
the day with most of my computers charged up and ready to go. I did
reduce my computer usage a fair bit and got a decent amount of reading
done this week, but I didn't enter complete computer asceticism. In
fact, dropping down to EWW for casual browsing (as I sometimes do at
summer cottages with poor Internet connectivity) and watching YouTube
videos through MPV goes pretty far. You can also enjoy PDFs for quite
a while without a significant drain on power, so both analog and
e-books were a good way to pass the time.

Also, smart phones are really energy efficient, amazingly so. If they
weren't hampered by a clearly predatory application ecosystem and a
generally hostile approach by mainstream websites, which constantly
try to drive you towards the horrendous apps, it would almost be quite
nice. Honestly, majority of apps could probably just die and be
replaced by simple websites or web apps we'd be much better off.

But yeah, in about a half-hour I'll get started with my livestream
again, which should have me set for two hours. Then it's just the last
two hours before clock strikes midnight and the self-imposed curse is
lifted. I'll see if I will stay up until then or just hop into bed and
read some more and just worry about the laptop/tablet/phone charger
tomorrow. USB-C PD is truly a wonder.

I will probably set up the power banks to charge off of solar tomorrow
even if I am not reliant on it. It's still "free power" after all and
I might as well use it when it's available.

** Power status:

- 95 Wh power bank: 0%
- 22 Wh power banks: 0% and <25% (estimated from power indicators)
- Phone: 79%
- Tablet: 63%
- Laptop: 93%

* Post scriptum

So, I decided to run the numbers on my electricity consumption from
the power company between the week before the challenge and the week
after.

During a fairly ordinary week, my electricity consumption was 52.73 kWh
and while I was using solar power for my computers, the figure was
46.95 kWh. I matched high-electricity consumption activities to the
best of my ability, meaning that I went to sauna twice during the
challenge as I had the week before. There may be differences in terms
of how much I ran things like the AC and how much I was using home
appliances, so these numbers aren't really scientific.

So, overall most of my power use would still be mostly tied to other
things that I am doing, but switching to solar power for my computers
has very likely a measurable impact. In this case that would be maybe
~10% reduction in power use, although don't cite that in a scientific
study. Technically the power bank I have could be used to run
appliances, since it does have an AC plug, but considering I already
ran out of power with just the computers I don't think the challenge
would have run for very long. Not to mention it probably cannot
sustain very high loads anyway, which is why I didn't even bother
trying to run my desktop off of it.

It should also be made clear that I put in enough caveats and
exceptions into this challenge to make it viable, so it's not like I
actually ran my computing entirely off of self-produced renewable
energy. This will also not last, since winter will make these panels
probably entirely useless for months. So, don't read too much into
this challenge beyond me just having some fun. If you want to actually
read about sustainable tech use then I would suggest reading
https://solar.lowtechmagazine.com/ instead.

However, like I said at the conclusion of the last day of the
challenge, I think I have picked up a couple of ideas from this and
will try to make use of the little solar array I have as supplemental
power. But, for now I think it's time to close off this blog post and
hit publish. See you on the other side!
