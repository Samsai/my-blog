---
title: "Annoyances with Game Stores"
date: 2019-01-29T17:30:00+02:00
tags:
  - gaming
  - linux
categories:
draft: false
---

I've occasionally complained on my livestreams about the state of the various online game stores that are out 
there and how they all carry annoying caveats that make it sometimes tricky to try and figure out where to
get your games. However, [a recent article on GOL](https://www.gamingonlinux.com/articles/putting-games-across-multiple-stores-is-not-easy-as-developers-keep-noting-recently.13421) made me decide to write down some of these
thoughts of mine here on my blog.

I am of course writing as a Linux customer exclusively, so I won't be taking into consideration services like
Uplay or Origin or Blizzard's Battle.net, since these historically have not mattered at all when it comes to
the sale of native Linux titles. I'll touch up on Epic Store, however, since it's a recent arrival and I have
other reasons to not be particularly happy about its existence.

## Steam, the bastion of Linux gaming

Steam has been the biggest source of PC games for many many years and when people talk about where they buy their
games Steam almost certainly comes up in the conversation. For us Linux gamers it's also remembered as the place
that pushed Linux gaming into the eyes of the mainstream through their Steam Machine and SteamOS initiatives.

So, it would be quite easy for us to regard Steam as a platform worthy of our respect. I certainly have for the
longest time considered them the only platform worth buying things from (except Humble, when their bundles had
interesting things to offer). However, over time my respect for Steam has pretty much dwindled.

The issues I have with it can be separated roughly into two categories: the pathological sloth of Valve and the
certainly less-than-perfect Steam client.

Let's address the client first, since it's quite simple to go through. Overall, the client is just not particularly
great. When it functions correctly it tends to mostly get out of your way and it even provides some useful aspects
such as the Steam overlay, which is helpful for monitoring framerates, alt-tabbing out of old SDL 1.2 games and
joining friends' online lobbies. When it doesn't work, it gets in the way of everything.

One problem I run into frequently is that when I change between network connections (I use a separate connection
to stream, for instance) Steam will happily continue working, except if I need any online features. This means
I will need to restart the client to get those features operational again. This also ties into my second gripe
with the client, which is that it's really quite slow, doesn't integrate well into my i3wm environment and
uses up just enough resources to annoy me while still not making me throw it out. And, while Steam itself is
not DRM, many games sold there essentially rely on it running which is certainly not a positive. It also has no
way (that I know of) to freeze a game's version, which would be handy for games that become incompatible with
old saves. Developers can try to counteract this effect by setting up "betas" of old versions, but then you
are relying on the developers, who may or may not do a good job at it.

Now, let's talk about one of the laziest gaming companies in business. Valve established itself as the go-to 
seller of PC video games, probably partially by accident, and they have been quite happy to do very little else 
except sit on a pile of money and let that pile grow. They've also continued making "improvements" that allow 
them to be even lazier. In the past Steam was a platform that only allowed games that exceeded some kind of a 
minimum bar of quality. Valve then decided to introduce the Steam Greenlight project, where the vetting of new 
releases would be shifted over to the community. The idea there was probably pretty noble, letting gamers decide 
how the platform would look etc.

After a couple hundred cases of developers buying Greenlight votes with game keys, the Greenlight project was
dismantled, replaced by the simple fee of $100 and your game would make it to Steam through Steam Direct. This
effectively meant that anybody who had the ability to put together a video game (this part is optional) and
a $100 in their pocket could ship something sort of resembling a video game on Steam. And since Steam was a
platform that devs wanted their games on, there were plenty of people willing to take Valve up on that offer.
The new releases page has never been the same since.

That's not to say that Steam doesn't get good games anymore, it's just so much more difficult to try and find them.
You essentially have to rely on word of mouth or websites like gamingonlinux.com to keep tabs on those games
because otherwise you are fairly screwed, whether you are a dev or a consumer.

## GOG, good not-just-old games

On the other side of the arena stands the platform that represents freedom from DRM. This has lead many Linux
gamers to swear by GOG. The problem here is quite simple: where Valve is lazy in general, GOG is lazy especially
when it comes to Linux. GOG took ages to begin supporting Linux and all throughout that time the Linux support
on GOG has been quite second-class. [The GOL article](https://www.gamingonlinux.com/articles/putting-games-across-multiple-stores-is-not-easy-as-developers-keep-noting-recently.13421) highlights this problem quite well:
where GOG has mostly shifted to its GOG Galaxy client and the associated APIs, the Linux client is still MIA and
the method of deploying Linux versions relies on old-school FTP file transfers. 

Some people consider the lack of a client to be a positive, however, it does significantly affect the general 
usability of GOG titles. If you are only interested in buying a single version of a game then you'd likely be 
just fine, but if you bought yourself a game from GOG that gets actively updated you are comparatively speaking
in a world of hurt. Modern games can be dozens of gigabytes in size and due to the lack of a smart updater,
if you need one of those games updated you are forced to redownload the entire game, which on capped or slow
Internet connections is a major hassle.

This technical limitation on the part of GOG is truly a sad thing, because I would honestly much prefer to buy
my games from GOG if even the simplest GOG updater was released, GOG can keep the social interaction features
of Galaxy to their Windows client for all I care. GOG's store credit system makes future purchases on GOG
quite appealing and the fact that I don't need to drag a client around with me every time I want to fire up
a game is also convenient. It's just that every time I want to buy something from GOG I need to think carefully
if I want to deal with possible redownloading of the game and game patches arriving later than on competing
platforms due to lacking developer tools.

## Itch.io, charming but small

I really like Itch, I seriously do. Almost everything I have complained about in this post, Itch does right. Their
developer tooling is described to be excellent and although I myself haven't used their Butler deployment helper
tool, I have worked on a project that was deployed using Butler and the person handling the deployments described
it quite positively. Their client is generally more responsive and integrates better in my desktop environment
than Steam and manages to do so while using less system resources. It's also optional and open source. Itch's
community is also quite vibrant and particularly the game jam features are quite fun. I myself have participated
in two Linux Game Jams, the creations of which you can see on [my Itch.io profile](https://samsai.itch.io/).

The biggest problem with Itch.io is its size and popularity. As an open platform, Itch has become a kind of
game developer's first publishing platform, which means that a large number of the games on Itch are fairly
low-quality and obscure titles. There are some bigger name indie titles being sold there but triple-I stuff,
not to mention publisher-backed, big-budget commercial titles are not present. This makes Itch a rather niche
platform, quite difficult to navigate and uninteresting to the mainstream.

So, in a sense Itch's state is also a bit of a tragedy. They support Linux as a first-class citizen with their
client, they are DRM-free, they have good developer tools but because they are small their selection of games
reflects their niche status. I would absolutely love to buy my games from Itch, but many times the games I am
looking to buy just aren't there and when they are, I often forget to check because I assume they won't be there.
And thus, the problem keeps feeding itself.

## So about that Epic Store...

The arrival of the Epic Store kind of kicked off this whole discussion about different game stores. Sure, we've
debated them in the past, but since Steam held what was essentially a monopoly on PC gaming, the discussions were
rather short-lived. Epic Store has set out to challenge that status quo and there's definitely a lot of buzz
around it.

Sadly, as it often is the case, Epic has totally overlooked Linux when it comes to their store. Maybe they see
Linux as too attached to Valve's interests, maybe they just don't care. Regardless, I don't expect Epic's store
to come up with any Linux support anytime soon. If it does, however, I expect it will probably be quite a bit
better than that of GOG's.

Why am I talking about it though? Two reasons.

Firstly, Epic Store may finally kick Valve off its bed and make them do something. And I certainly do hope they
do something soon, because Epic is picking up store-exclusives left right and center. Which leads me to my second
point...

I really quite loathe Epic Store at the moment. Not because they don't have Linux support or because I'm one of
those people who feels they only ever need one place to buy, store and manage their games. I don't like them
because they brought Supergiant Games' Hades into an exclusivity deal. I am hoping that the deal is only for the
duration of their "early access" period because otherwise Hades will likely be the first game from Supergiant that
I will not be playing. And considering how much of a fan of Supergiant I am, that definitely stings.

But, in general, I really don't like how Epic seems to want to compete on exclusives. I am of the opinion that
exclusives are not a nice business model and that platforms should compete on their merits and not on the number
of special deals they've struck with game studios. I would like to have the power to make my purchasing decisions
on a platform that suits my needs and not be forced to use another simply because they happened to make it 
impossible for me to use whichever platform I wanted to use.

I mean, one of the reasons I started using Mastodon was because I felt that in a federated social network I could
pick which tools I wanted to use and not be cut-off from the people that decided to make a different choice. Now,
I don't think a federated game store will become a thing, but I certainly would still like to have options when
it comes to where I can buy a game from.

## What could be done about it

So that I don't sound like I am just complaining, I figured I'd come up with at least a couple of things that
could be done to help make the situation a little bit better.

Firstly, if you happen to be a game developer, I'd personally recommend you also ship your game on Itch if you can.
The platform is quite nice and it would be very nice to see it grow to be a more mainstream option for buying
games. Also, make sure that people also know that your game is on Itch.

Secondly, I would kind of hope that more games came with some kind of a built-in updater tool. I don't mean a
full-blown, special-purpose launcher/game store combination, just some small tool either in a launcher or in-game
that would let me handle updates to the game without being tied to what the store offers. I am aware that this
kind of a tool would require a fair bit of infrastructure to make it work, thus putting it out of reach of
smaller indie developers, but I do think it would both help with deploying games to multiple store fronts, since
you don't need to keep each store version 100% up-to-date always, and it would put my mind at ease if I were to 
buy the game from GOG, where handling updates for me would involve redownloads and manual work.

And finally, Epic, please stop.
