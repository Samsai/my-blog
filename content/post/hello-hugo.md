---
title: "Hello, Hugo"
date: 2018-12-03T16:18:15+02:00
tags:
  - meta
categories:
draft: false
---

So, I decided to finally put together my own website. Took me a while to figure out which tools
I wanted to use to make it happen, but after some thinking I settled onto Hugo, since
it seems to be a fairly easy static site generator and I want to keep things light-weight.

On this blog I am planning to write about all sorts of things that I can't find any place for
elsewhere, but in general I'll be rambling about Linux, gaming, streaming and programming on
this blog. I'll try to have it tagged properly so that all those things can live side by side
without massive issues.

It's quite nice to finally have a slice of the Internet that is just mine and nobody elses
and not have to rely on someone else, except on the people that keep the cogs turning, of
course.

So, fun times ahead. We'll see how often I will actually write here but I do expect
to be writing some mad scribblings here at some point in the near-future.

See you then!
