---
title: "Introduction to Gemini and the Small Internet"
date: 2020-05-17T11:15:26+03:00
tags:
  - tech
  - meta
categories:
draft: false
---

I recently read about Gemini, a new internet protocol that aims to do things kind of
similar to the web but while also being more simple. I found it quite interesting and 
ended up spending a good while exploring the small constellation of servers hosting
content over Gemini. I've found it quite refreshing and nice and would recommend
checking it out.

However, while talking with a friend (hi, Nezchan!) I noticed that the Small Internet
can be a bit tricky to figure out, so I've decided to write this post as an introduction
and a beginner's guide to accessing content on Gemini (and Gopher).

## The Small Internet

The World Wide Web is the standard "Internet" we are most familiar with. The WWW is
built on a protocol called HTTP and web documents are often served in the form of 
HTML, along with CSS for styling and Javascript for interactivity.

The Small Internet, as I've seen people call it, is built on alternative protocols.
Previously that meant a protocol called Gopher, which was popular in the 90s but ultimately
lost the battle to HTTP. It has, however, maintained a small user-base of hackers and hobbyists
who enjoy the technical simplicity and text-oriented nature of Gopher. Recently a new
protocol called Gemini was collaboratively designed as a kind of middle ground between
the simple Gopher and the more complex HTTP. It aims to better serve certain use-cases
that Gopher cannot quite fulfill while still keeping things simple compared to
the WWW. For example, Gemini mandates the use of TLS to encrypt traffic between a Gemini
server and a client and supports MIME-types, so servers can better instruct clients to deal
with different types of files. Generally speaking Gemini and Gopher seem to be co-existing 
peacefully and many Gopher clients have added support for Gemini as well. It's also not unusual to
link to Gopher content from Gemini sites.

The Small Internet is in some ways similar to the Big Internet we know. It consists of
servers, from which people serve documents and files. People host their own journals on
it (called "phlogs" on Gopher or "gemlogs/flight journals" on Gemini) similar to how 
people host blogs on the regular web. There are search engines and content aggregators.
Some people even mirror web content on the Small Internet, you can for example read
Reddit on an unofficial Gopher mirror.

Where the Small Internet differs is in presentation. Pages are mostly plain-text, you
cannot serve scripts to your users and you cannot embed images into pages directly.
This means that Small Internet pages tend to be relatively snappy and simple compared
to their WWW counterparts. That doesn't necessarily mean they are ugly, however.
ASCII art is frequently used to spice up pages in lieu of style sheets and banner
images.

## Gaining access

Accessing the Small Internet isn't nearly as straight-forward as accessing the regular
web. Your regular web browser likely doesn't support the protocols needed. I'm going
to go over a couple of clients you can use to access Gemini and Gopher content.

### Web proxies

The easiest way, which doesn't require installation of any client software, is to
use a web proxy. These are basically just web applications that allow you to view
Small Internet content. I don't think they are particularly amazing and they come
with limitations, but they are an easy way to get started.

A couple of Gemini proxies exist:

- https://proxy.vulpes.one/gemini/gemini.circumlunar.space
- https://portal.mozz.us/gemini/gemini.circumlunar.space/

Floodgap also maintains a Gopher web proxy:

- https://gopher.floodgap.com/gopher/gw

### Castor, a graphical Gemini/Gopher client

Castor is a graphical client for Gemini and Gopher written with Rust and GTK.
It's probably the most user-friendly way of accessing the Small Internet due to
it's modern, browser-like interface. Links will be visible as buttons.

![Castor client](/img/gemini/castor.jpg)

Castor can be found over at https://git.sr.ht/~julienxx/castor along with the
installation instructions. For Ubuntu the installation process would look
a little something like this:

```bash
  # Download the source code using Git
  $ git clone https://git.sr.ht/~julienxx/castor
  $ cd castor
  
  # Install dependencies
  $ sudo apt install build-essential rustc cargo libgtk-3-dev libgdk-pixbuf2.0-dev libssl-dev
  
  # Build and install Castor
  $ make
  $ sudo make install
```

Arch users get away with a bit easier, since Castor can be installed from the [AUR](https://aur.archlinux.org/packages/castor-git/).

Castor's colours can also be tuned in the settings file. Castor's web page
above has instructions for how that can be done. My ```~/.config/castor/settings.toml``` file
looks as follows: 

```toml
[general]
start_url = "gemini://gemini.circumlunar.space"

[colors]                                                                                                                               
  h1 = "#24F866"                                                                                                                       
  h2 = "#66ff66"                                                                                                                       
  h3 = "#66ff66"                                                                                                                       
  list = "#55ff88"                                                                                                                     
  text = "#55ff88"                                                                                                                     
  background = "#00230F"

[monospace]
gemini=true
gopher=true
finger=true
```

Castor is generally a pleasant way to browse the Small Internet. It has some stability issues and may
crash sometimes but the crashes are not random but instead related to certain servers or doing
certain actions on these servers.

### Bombadillo, a terminal Gemini/Gopher client

Bombadillo is a Gemini/Gopher client designed to run in a terminal. It also uses
keybindings that are familiar to Vi/Vim users.

Bombadillo can be found over at http://bombadillo.colorfield.space/ with
build instructions. However, it's easier to just use the pre-built binaries:
http://bombadillo.colorfield.space/releases/binaries.html

Bombadillo's keybindings may take a bit of getting used to, if you aren't
already familiar with Vim. Here's a list of basic keys and commands to
get started:

- "q" to quit Bombadillo
- "j" and "k" will scroll down and up
- "b" to go to previous page, "f" moves forward in history
- ":" or Spacebar (referred to as "SPC") enters command mode 

To access a Gemini site you first need to enter command mode and then type
in the address and press Enter:

```SPC gemini://gemini.circumlunar.space<ENTER>```

Bombadillo uses a number-based system for accessing links. Each Gemini or Gopher
link on a page is assigned a number, which is used to refer to that link. A link
on a Gemini page might, for example, look like the following:

```[3]   Have free words of wisdom ```

To access that link, enter command mode, enter the number of the link and press Enter:

```SPC 3<ENTER>```

Bombadillo, as a terminal-based client, is particularly well-suited for use in
[Cool Retro Term](https://github.com/Swordfish90/cool-retro-term)

![Bombadillo in Cool Retro Term](/img/gemini/bombadillo.jpg)

The main problem with Bombadillo is that it seems somewhat strict regarding the TLS
certificates on servers. Technically this isn't a fault of Bombadillo, it just happens
that many of the Gemini sites have outdated or otherwise incorrect TLS certificates.
You may need to use another client in conjunction with Bombadillo to access all the
content on Gemini though. Castor is much more lenient in this regard.

### AV-98

AV-98 is a primitive but pretty effective Gemini client. It doesn't speak Gopher itself,
but you can browse some Gopher content through a Gemini->Gopher proxy. It's available
on https://tildegit.org/solderpunk/AV-98 and basically just requires Python 3 and
Sqlite 3, making it easy to run. I had to make sure I created ```~/.config/av98``` 
directory first before it agreed to launch, but after that it ran fine.

AV-98 has a number of commands that are used for navigation. A pretty good way to get
started is to just type ```help<ENTER>``` and do a bit of experimentation to get going.
Personally, AV-98 is a bit cumbersome and I seem to get lost on which page I'm on,
but the commands are user-friendly and the setup process easy.

### Other clients

The main Gemini site has a list of other Gemini clients: https://gemini.circumlunar.space/clients.html 
I've tried a few of these and they seem okay with some caveats. Elpher (a Gopher client for Emacs) seems
to do fine on Gopher sites but struggles with certain Gemini servers. 

**Update:** After having been contacted by the Elpher developer and doing a bit of debugging,
it seems like this is a problem related to my Doom Emacs configuration and not Elpher. If you
are using Emacs with your own configuration it probably works just fine with only minor missing
features.

**Update 2:** Elpher has now been patched to work correctly under Doom Emacs as well. I can now
highly recommend Elpher as a Gemini and Gopher client if you already happen to be an Emacs user.

Asuka, made by the same person as Castor, is okay but IMO doesn't look as neat as Bombadillo.

However, if Castor and Bombadillo aren't for you, go through the options on the site and see
if any of the other clients are for you.

## What is there to do on the Small Internet?

Like I mentioned before, Gemini and Gopher are highly text-oriented, so most of the
content is text. Plenty of people write blog-style journals on the Small Internet.
So, pages on the Small Internet are kind of like this website, mostly textual and
relatively static.

However, the Small Internet can do a bit more than just host static text documents
too. You can share all sorts of files over Gemini and Gopher. Some people share
post their own mixtapes on Gemini (gemini://konpeito.media), others have libraries
of PDFs. Gopher and Gemini can also be used to host games and apps, for example
you can practice a bit of virtual botany (gemini://astrobotany.mozz.us).

So, the only real limits are those imposed by the protocols, and the protocols
allow for plenty of creativity. The best way to see what you can do on the Small
Internet is to go an experience it yourself.

The Small Internet is, like you might tell from the name, pretty small. However,
it is also relatively tight-knit and well connected. A few hubs exist that provide
good starting points for browsing sessions.

### gemini://gemini.circumlunar.space

The main hub of Geminispace. They have a few pages about Gemini itself, along with
links to various Gemini-related software projects and the Gemini mailing list, where
development on Gemini itself takes place. There's also a list of known Gemini servers.

In addition to that they have a link to a Gemini search engine called "Gemini Universal
Search" (aka GUS) and two Geminispace aggregators: CAPCOM and Spacewalk. These aggregators
track Gemini journals ("gemlogs" or "flight journals") and provide a timeline of content.

### gopher://floodgap.com

On the Gopher side of things, Floodgap seems to be a pretty big hub of activity.
They offer lists of Gopher clients, known servers lists and a Gopher search engine
called Veronica-2. There are also weather and news mirrors, file archives and
other such things.

### gopher://i-logout.cz/1/bongusta/

Bongusta is a content aggregator for Gopherspace. It tracks Gopher logs ("phlogs")
and provides a timeline of journal entries. It's a bit more populated than CAPCOM
or Spacewalk on Gemini, and is definitely a good place to look for interesting
journals.

### Pubnixes and Tildeverse

Many of the Gopher and Gemini sites are operated as pubnix systems of some sort.
They are publicly available Unix/Linux systems that provide their users access
to a Unix shell for free. They also often offer users a Gopher or a Gemini space.

SDF is one of the bigger public Unix groups over at gopher://sdf.org and
has a pretty big number of users. On the Web side of things, you may also
want to check out https://tildeverse.org/ which lists a number of so called
"tilde communities" which have different goals and provide different things.
Even if you aren't too interested in gaining membership, I recommend
checking out https://cosmic.voyage/ which is a collaborative science fiction
community of writers.

### gemini://samsai.eu

I got pretty excited about all this Gemini stuff, so I decided to set up my own
Gemini server. It has a "super secret gemlog" which I'll probably be using
in conjunction with this regular blog. My current plan for it is to host the kind
of stuff that is longer than a tweet but less developed than a blog post over
on the Gemini server. I might also try some more experimental stuff there, which
I would feel uncertain about posting here. I also have a little bit CGI scripting
in place to do some semi-dynamic content on the site and I might do more of such
CGI experimentation if I happen to come up with fun ideas for it.

### ... and so much more!

There is plenty more that I didn't touch on here. People have set up all sorts
of apps, mirrors etc. on both the Gopherspace and the Geminispace. There's plenty
to find and hopefully the constellation of different servers continues growing.

The links mentioned here should hopefully provide a pretty good springboard to 
dive into the realm of plain-text.

Hopefully this post has been useful as an introduction to the weird and
wonderful world of Small Internet. 
