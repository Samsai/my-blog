---
title: "Artifact's CCG Lootboxes"
date: 2018-12-06T11:24:49+02:00
tags:
  - gaming
categories:
draft: false
---

Valve released their own collectible/trading card game Artifact somewhat recently to a
moderate community backlash due to the game's cost and additional in-game booster packs,
which the player can buy to gain additional cards. 

The lootbox mechanic wasn't exactly a surprise, considering the kind of game we are talking about, 
but due to AAA publishers pushing lootbox monetization into every video game imaginable, they don't
exactly have a fantastic reputation among gamers, as many see them as psychological manipulation
and borderline gambling.

However, many people have defended Artifact and said that the game is fine as it is, since
real-world trading card games work more or less with the same model, so Artifact isn't
doing anything more wrong than those games.

Now, I have not played Artifact, nor do I intend do, but I figured I'd write down some
of my thoughts on the whole lootbox thing and let you agree or disagree. Okay? Okay.

## "I can't believe it's not gambling!"

More and more people have been contemplating if lootboxes count as gambling. Gambling commissions
in multiple countries are examining them right now and some countries have already decided that
lootboxes in video games are in fact unlawful gambling, which has lead to some developers,
like Valve, to pull some of these features in these countries.

I myself tend to agree that lootboxes are indeed gambling. I am not much a consumer of lootboxes,
but at one time I did get into them for a brief period and I only stopped because I realized
that I was spending way more money than I expected and because I noticed that the way I dealt
with lootboxes wasn't exactly healthy. 

I specifically began opening CS:GO and TF2 lootboxes and eventually I was considering buying keys an 
"investment" that might "pay-off" if I got a good drop out of the lootboxes. I stopped because I 
realized that the odds are stacked against you in such a way that in general your investment can't pay off. 
You know how some say the lottery is a voluntary tax for the people who can't do math? Yeah, slot machines 
and lootboxes are basically the same thing.

So, ultimately there really isn't much of a difference in a slot machine and TF2 or CS:GO
lootboxes: you put in some money on a chance to win something of value, with the very real
possibility of not getting back the same value you put into the machine. Games like TF2 and
CS:GO also have an easy method of cashing out through the community market, which is
willing to put absolutely insane prices on some of these pieces of polygons and pixels.

![CS:GO Skin-sanity](/img/csgo-skinsanity.jpg)

So, how does Artifact fit into this?

## It's fine if it's trading cards

Many, if not most, trading card games in the real-world operate with a similar booster pack
mechanic to that of Artifact, which is an argument many have used to defend Artifact
lootboxes. After all, if trading cards can do it, why not Artifact?

That is actually a fair point in that there isn't really much of a difference between
how the mechanic works in Artifact and, say, the Pokemon card game. You buy a pack,
the contents of which you cannot be sure of until you open it. Some cards are highly
valuable and others are essentially worthless.

This made me wonder, what would the reaction be if these traditional trading cards were
sold in transparent packets, so that the consumer would always know what they were
buying? I think this is an interesting thought experiment, because to me it intuitively
would seem like traditional trading cards are not really that different from gambling.

I am willing to bet that most people who open these packs get a thrill from the
random chance of either getting something totally worthless or something of value.
So, people might even find these transparent packets boring. It would no doubt
also lower the profits for the companies making these cards, since the packets with
common or weak cards would be left on the store shelves.

So, CCGs and TCGs are basically gambling, and after some looking around on the
Internet I found many people agreeing that they basically are gambling, just that
the harm they cause is relatively minor to various forms of hard gambling. So,
maybe Artifact is just fine as it is?

## Just a press of a button away

One major difference between Artifact and traditional TCGs is the way you access
the game. Artifact is a piece of Internet-connected software, anything you might
want to do with the game you can do from the safety of your own house and your
own room. 

Contrast this to a CCG that might require you to walk to the nearest store to
make additions to your card collection and the trips to your friends or where-ever
you might actually play the game itself.

Artifact let's you do all of that without you having to move a centimeter away
from your computer. This means that the barrier to spend your money on
booster packs is severely lowered, you just need to click a button and possibly
input your CC details and bam, you have more fuel for your brain to enjoy. It's also
worth noting that these purchases are made in multiple small pieces and thus your ability to
track how much money you have put into the machine is hampered, so the sums can start climbing 
at a surprising rate if you don't pay attention.

Some people have commented that thus far the lootboxes haven't felt necessary and that the
prices of the Artifact community market have been more or less sane. However, I feel this doesn't
properly take into account the fact that Artifact is extremely new and nobody knows how to play
the game yet. What I mean by that is that not only have the optimal card combinations not been
figured out, but that more cards are bound to arrive later and the power of those cards is yet
unknown. And don't be fooled for one second, you are buying power when shopping on the community
market or opening lootboxes. Valve isn't known for making balanced game changes and with the
money they make from lootboxes and the community market sales, it's in their best interests
to break up the meta of the game every now and then with new card additions.

## Conclusions

I am not saying that Artifact is a bad game, I imagine if I were a person who was in to
multiplayer CCGs I would probably be drooling over it too. However, what I wanted to point
out with this article is that we should be aware of what kinds of psychological tricks are
going on when you buy lootboxes, even when you are buying them in the form of a booster pack
for a CCG.

It's also worth keeping in mind that just because these kinds of booster packs have been done before
doesn't mean that doing them now is not just as potentially harmful as they would be otherwise.
In fact, I think if this lootbox craze keeps going the way it has, we might even want to investigate
if the soft gambling of physical lootboxes should be reassessed too. I personally would much
prefer if all of these CCGs sold their contents transparently rather than in an opaque package.

Finally, I would like to point out that these kinds of booster packs aren't necessary for CCG
experiences. [Argentum Age](http://www.argentumage.com/) is an open source CCG that doesn't do
booster packs or in-app purchases of any kind. They are coming to Steam too in the near-future, which
will be quite exciting to see. Then there's of course my favourite card game: [Slay the Spire](https://store.steampowered.com/app/646570/Slay_the_Spire/)
which not only doesn't have booster packs or in-app purchass, but also gets rid of the pesky randomness
that comes with playing with other humans.
