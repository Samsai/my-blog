---
title: "Organizing YouTube subscriptions in Newsboat RSS reader"
date: 2019-11-03T13:19:27+02:00
tags:
  - linux
categories:
draft: false
---

Recently YouTube decided to roll out (for a brief moment) an "update" to their home page
design that turned the recommendation page's grid layout into an endlessly scrolling
feed, which was clunky and annoying to use and overall sucked. They did claim that it
was a technical issue and eventually (like, after an entire day) rolled it back to
the grid layout. This time.

If you've been on YouTube for any significant amount of time, you will probably be aware
that the YouTube web design process seems to be a careful mix of poor choices and making
random changes that seemingly nobody wants. I don't remember many YouTube design overhauls
that have been remembered fondly and generally people seem happier when the YouTube
design team is not working.

I could rant plenty more about all the horrible choices and practices of YouTube and
Google, but in a nutshell this particular "technical issue" annoyed me enough that
I decided to try and de-Googlify my YouTube subscriptions a bit. I'm still tied to
the YouTube ecosystem in the sense that the videos I watch will still be on YouTube,
but at least I can spare the clicks to the YouTube front page.

## RSS and YouTube

Helpfully, YouTube provides access to RSS feeds to individual channel's content
and they also offer an OPML file that you can import into your RSS reader to
read all of the channel feeds for the channels you have subscribed to on YouTube.

In fact, for some feed readers such as Liferea, all you need to do is go to
[the YouTube Subscriptions Manager page](https://www.youtube.com/subscription_manager) and
click the "Export subscriptions" button at the bottom of the page to download the OPML
file, which you can then import in Lifera and Liferea will create a meta-feed containing
all of the recent videos from your YouTube subscriptions.

However, I happen to use Newsboat as my RSS reader because it happens to fit my more
keyboard-oriented workflow a little bit better, which doesn't make this quite that
easy. So, I'm going to document the path I took to organizing my feeds in Newsboat,
since I had to combine a few pieces of information here and there to get it working
my way.

## Enter Newsboat

Newsboat also has the ability to import an OPML file, but the annoyance is that it
will import all the channel feeds as individual feeds, which quickly clutters up
the reader if you have more than a couple of subscriptions. However, this functionality
is good enough that we can build upon it.

The YouTube channel feeds can be imported into Newsboat with the command:
```bash
    $ newsboat -i <path-to-OPML-file>
```

This will add all of the feeds to the ```~/.newsboat/urls``` file but at least for me
the next task was preventing the feeds from entirely taking over my RSS reader.

Luckily Newsboat has a few features that help you manage and organize your feeds.
Of particular use here are the feed hiding and tagging tools.

By default the YouTube channel feeds will be tagged as "YouTube Subscriptions", which
is defined by the string after the feed URLs in the ```~/.newsboat/urls``` file. Feeds can
also have multiple tags and you can add these tags by modifying the urls file with
a text editor.

An example of tagging a feed might as follows:
```
    https://samsai.eu/index.xml "linux" "gaming" "ranting about things"
```
This would apply the tags "linux", "gaming" and "ranting about things" to the feed
for this particular blog.

Next, you can hide feeds by adding the "!" tag to a feed. This will make the feed
disappear from the visible list of feeds on Newsboat:

```
    https://samsai.eu/index.xml "!" "linux" "gaming" "ranting about things"
```

Obviously this is not particularly helpful yet, since we do want to still read
the feeds, we just don't want them cluttering the place. Worry not, for now
Query Feeds will come to the rescue.

Query Feeds in Newsboat allow you to combine feeds based on various filtering
criteria into meta-feeds, which act like any other feed on Newsboat. In our
particular case we want to combine together all of the feeds from our YouTube
subscriptions, which have already been tagged for us,

So, to create a query feed, we just need to add a line to our urls file that
defines the filters we want to use to construct the feed. I am not yet an expert
on how the filters are properly used in Newsboat, but to create a query feed
that contains all of YouTube Subscriptions, this should suffice:

```
"query:YouTube:tags # \"YouTube Subscriptions\""
```

Now we will have a feed that contains all of our YouTube subscriptions and
the individual channel feeds won't clutter the interface, hooray! The only
problem is that the process is really annoying and includes a bunch
of really repetitive actions, such as adding "!" to every YouTube feed
individually.

## Making things easier

People shouldn't do repetitive tasks, those are best left to machines.
So, I created a short Bash script that will clear the previously imported
YouTube feeds, reimport them from the OPML file, tag them accordingly
and create the meta-feed for all of our YouTube subscriptions:

```bash
#! /bin/bash

# Delete all previous YouTube feeds
# including the query feed
sed -i '/youtube.com\/feeds/d' ~/.newsboat/urls
sed -i '/query:YouTube:tags/d' ~/.newsboat/urls

# Import the new feed
newsboat -i $1

# Create a query feed to combine the youtube feeds
echo '"query:YouTube:tags # \"youtube\""' >> ~/.newsboat/urls

# Hide the individual feeds and tag them under "youtube"
sed -i -e 's/"YouTube Subscriptions"/"!" "youtube"/g' ~/.newsboat/urls

```

Note that I am no expert at using sed, so the commands I have given here
could probably be combined together for efficiency.

This script can then be simply run as follows:
```bash
    $ bash ./update-youtube-subscriptions.sh <path-to-youtube-subscriptions-opml>
```

The reason that I first delete the previous YouTube feeds is to make it easier to
unsubscribe from channels. Importing the OPML file from YouTube will not clear
any channels, only add new ones, and it gets really annoying to try and figure
out which feed belongs to which channel. So, clearing the subscriptions and
then adding them back from the OPML just makes that part easier.

I also delete the query feed and re-add it back because the import process seems
to break the query feed for whatever reason.

This script now should now make it a bit easier to subscribe to new channels and
unsubscribe from channels you no longer wish to watch. Simply subscribe to the
channel or unsubscribe from it on YouTube, go to the Subscription Manager and
redownload the OPML file and run the script on that file.

## Bonus functionality: watching YouTube videos with MPV

Now that you have replaced YouTube's subscription feed with Newsboat (which probably 
is more reliable than the actual YouTube subscriptions feed itself), why not cut YouTube 
out of the equation even more?

The MPV media player allows watching YouTube videos without even opening the video
page by simply supplying the URL. This doesn't work with all videos and YouTube
typically tries to break this kind of functionality every now and then, but it
works most of the time if you keep your MPV and its dependencies up to date.

Newsboat allows defining macros in the ```~/.newsboat/config``` file and we'll quickly
define a macro that will temporarily replace the browser Newsboat will use to open
links with MPV and then switch it back:

```
browser "xdg-open %u"
macro m set browser "mpv %u"; open-in-browser ; set browser "xdg-open %u"
```

I've used xdg-open in this case as the browser, which hopefully runs your
preferred browser, but you can replace it with your own favourite web browser.

Now, when you have selected a video from the YouTube subscriptions meta-feed
you can press ```,m``` (note that the comma indicates you want to run a macro) 
and the URL should be opened with MPV.

I personally don't really use this macro because MPV has some annoyances, such
as wanting to open the video at the highest possible quality setting, but
I think it's a fun little feature purely because the combination of Newsboat
and MPV allows you to watch YouTube without ever actually going to YouTube.
