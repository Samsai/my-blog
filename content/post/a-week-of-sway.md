---
title: "A Week of Sway"
date: 2019-03-28T12:04:18+02:00
tags:
  - linux
categories:
draft: false 
---

I've recently been using the Sway Wayland window manager/compositor and I figured I'd share some of my thoughts
on it after a little over a week of use.

Wayland has been long time coming but only fairly recently have we started to see Wayland compositors that are
generally usable. There have been experimental compositors used mainly for testing, like Weston for example, but
they haven't really been fully usable as a day to day solution. Or at least I haven't heard of anyone running
Weston as their primary desktop environment. Nowadays Wayland compositors have luckily started becoming more
widespread and KDE's KWin and GNOME's Mutter, along with smaller Wayland compositors like Sway have started to
stabilize so that some have already switched to them as their daily driver.

I have been following Sway's development with great interest, since I have never really been a KDE person, and
GNOME is, at least in my opinion, poorly designed to work in Wayland. I've used GNOME and while it is probably the
most advanced Wayland compositor out there, it has issues that make me really not want to use it. I'll rant about
that a bit later. Another reason for my interest in Sway is that I've been using the i3 window manager for a
good while now and I absolutely love i3's keyboard-oriented workflow. Sway attempts to be, essentially, a drop-in
Wayland replacement for i3, so I'd get to keep my workflow even if I have to jump between the two.

## Quick intro to Wayland

In case you aren't really sure what Wayland actually even is, it's basically a protocol that was designed to
replace the current window system used on Linux, X11. The X Window System is an old beast, hailing from
the dark age of the 1980s. As such, it contains a lot of code that is no longer relevant or maps poorly to
modern systems and modern requirements. While it is still being updated and cleaned, the developers themselves
have stated that it is due for a replacement.

Wayland aims to be that replacement and provide a clean break from the legacy code. It also combines some tasks
that in X11 are handled by separate programs. Your typical X11 desktop environment runs the X11 server, which
provides windowing, a window manager which allows you to more easily move and manage these windows and optionally
a compositor, which is responsible for providing various window manager effects, such as transparency and fade
animations, not to mention silly stuff like wobbly windows. In Wayland, the windowing system, window manager and
compositor are all one and the same. This means that in Wayland, data doesn't need to pass through multiple
applications so that you can manage your open application windows. This also means that each Wayland compositor
is its own independent Wayland implentation, and in fact there isn't any kind of official Wayland server that
you can run on your system.

## My general workflow

In order to give you an idea what I used Sway for, I figured it would be a good idea to go over some of the ways
in which I use my PC.

As a computer science student, a decent bit of my week is spent working on code. Through the use of i3, I started
adopting more keyboard-focused applications across the board and I ended up on Vim and then Neovim for my general
purpose coding. Java plays a fairly big part in my studies too, though, so I also have a Netbeans IDE handy, since
I haven't quite managed to set up my Vim to provide a reasonable environment for Java development.

I also play video games quite heavily, which also goes along with my weekly livestreaming to Twitch.tv. This
naturally means that I need to be able to at least capture the screen using OBS at a reasonable framerate to
stream it over to my viewers. I also sometimes edit video I've captured from streams using Kdenlive to upload
as a VOD.

Rest of my computer usage is fairly ordinary, I have my chat programs like IRC and Mumble running, along with
applications I'd rather get rid of, like Discord. I also naturally browse the Interwebs fairly regularly on
Firefox.

## Setup

Since Sway is designed as a drop-in replacement for i3, I just copied my i3 config file to ~/.config/sway/ and
booted up Sway. Almost everything, except some window title bar colouring worked without a hitch, although some
small tuning here and there was necessary. For example, I added some keyboard configurations to the config file,
since I cannot rely on Xorg setting my keyboard layout correctly.

Another thing was that by default my system defaulted to the Xfce notification daemon, which drew the
notifications in a window at the center of the screen which was distracting and annoying. Luckily there exists
a Wayland notification daemon called [mako](https://github.com/emersion/mako) which is almost, but not quite
perfect. The biggest flaw it has is that it doesn't have the option to automatically dismiss notifications
after a certain amount of time. I might open an issue about it or try and delve into the code to see if I can
make it do that myself. As a temporary solution I bound Meta-m in my Sway config to run "makoctl dismiss" to 
dismiss notifications quickly. Not an optimal solution but it gets the job done.

I also had to do a little bit of tweaking with my status panel. I've used Polybar with my i3 setup, but that
naturally needed to be replaced. Sway comes with swaybar, which is fairly close in function to i3bar and I was
able to reuse my i3blocks configuration with swaybar directly. There is also a project called waybar, which aims
to create something akin to Polybar, but it didn't have all the necessary scripts by default (namely a disk space
indicator) and I wasn't in the mood for coding my own, so I just went with swaybar and i3blocks. A limitation of
this setup is that swaybar's tray icon support is still work-in-progress, so some icons such as the NetworkManager
applet and the Xfce 4 Notes applet don't show up. I replaced the nm-applet with just nmtui for network management
and created a quick alias that opens my notes in Vim, which works around the issue. I briefly tried waybar and its
tray icon support seems more complete, but since these were fairly small problems, I wasn't too annoyed by this.

## Sway in general use

If you've used i3 WM, you'll basically be right at home in Sway. Only in a couple of minor cases has Sway's behaviour
differed from that of i3 and not in a disruptive way. It has been able to replicate my typical usage behaviours
excellently, although I tend to keep things fairly simple.

![Empty desktop on Sway](/img/wayland/sway-empty-desktop.png)

Sway is also quite snappy, and at least anecdotally I'd say it's more responsive than i3 on Xorg. Windows just seem
to appear and respond to keypresses and mouse clicks faster. Particular thing I noticed is that when I fire up dmenu
on Sway, it seems to be ready to go quicker than on i3. When opening windows in i3, there was also frequently a split
second of garbled texture data visible on screen, which doesn't seem to happen on Sway.

![Coding in Vim on Sway](/img/wayland/sway-coding.png)

Sway isn't a totally problem-free experience, though. To get Netbeans (ugh) to run, I had to set the environment
variable _JAVA_AWT_WM_NONREPARENTING=1 before Netbeans would render the window correctly. After setting the environment
variable, it worked just as well as you'd expect Netbeans to work. Another, similar problem was with the Thunar file
manager, which kept crashing constantly until I told it to use the X11 backend with GDK_BACKEND=x11. Some games also
had minor issues, like OpenMW refusing to launch if anti-aliasing was enabled. This problem also seemed to be present
with a couple of other games, and is apparently linked to xwayland, the Wayland X11 compatibility layer, being unable
to support certain kinds of window creation requests tied to anti-aliasing. The biggest issue I've had thus far is
with Steam, which doesn't seem to respond to mouse clicks in various pop-up dialogs. This is not exactly surprising,
since Steam doesn't really play nice in i3 either, but it does harm the usability of Steam to some extent. Luckily, 
where the mouse clicks fail, pressing Enter seems to do the trick.

In general, however, using Sway was quite painless and none of these issues took too long to debug. It just seems that
for some applications moving from X11 to Wayland reveals some teething issues that are hopefully going to be taken care
of by, either by modifying xwayland or fully moving these applications over to Wayland. Apart from the Steam mouse-click
issue, I've been able to browse my webs, do my university-related programming exercises, play some video games and edit
some video without having to give much thought to what windowing system I'm using.

## Livestreaming and the tale of two OBS plugins

Wayland doesn't have an official protocol for screensharing or taking screenshots. However, many Wayland compositors
have implemented their own protocols as a way to create screenshotting programs and the like. The library which Sway
is built around, wlroots, has a protocol called wlr_screencopy_unstable_v1 which allows you to query the compositor
for the current frame. This makes it possible to write screenshot applications, like [grim](https://github.com/emersion/grim).
Another application, [wf-recorder](https://github.com/ammen99/wf-recorder), used the same protocol to create a
screencasting tool, but this tool alone does not a livestream make. It did provide a useful starting point, though.
So, I essentially took the source code of wf-recorder and worked it into the form of an OBS plugin. You can find the
(ugly) source code of my plugin [on my Gitlab](https://gitlab.com/Samsai/obs-wlr-capture).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/DKbdaKHMtDw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

However, I wasn't the only one who apparently came up with the idea of using this protocol, since another one was
posted on the OBS forums just about a week before I managed to get my plugin running to some extent. This plugin,
[wlrobs](https://hg.sr.ht/~scoopta/wlrobs) was and is currently more advanced in terms of display handling and
properties than mine, and I would recommend it over my plugin. However, having wasted some time trying to sort
out pointer weirdness, I've grown a bit fond of my plugin and I'll likely keep using and developing it in parallel,
most likely by taking some creative inspiration (and straight-up source code) from wlrobs.

Regardless, with the OBS plugin in a usable state, I've actually done a couple of livestreams already from within
my Sway session without any notable technical hick-ups and according to my viewers, the framerate and quality of these
streams has been up to par in the video department, so I'd call it a resounding success. Naturally, these OBS plugins
do have the limitation of now being able to capture individual windows, rather the entire screen gets captured at the
same time. If a protocol eventually gets made that allows you to tap into per-window buffers, it might be possible to
implement a window-capture plugin but right now it isn't easily achievable.

## Why Sway over other Wayland compositors

I mentioned I'd rant about GNOME later, so now it's time for that. As I mentioned, multiple Wayland compositors exist
and GNOME's Mutter and KDE's KWin are examples of them. I have only used KDE very briefly, so I cannot really say
much about it other than that's not really for me. GNOME, however, I used for a few months when I decided to do my
original little Wayland experiment. Many people have bashed GNOME and I didn't really find the workflow that intuitive
either, but over time it did grow on me. Nowadays I'd probably have a horrible time in GNOME, because I'm so used to
tiling window management and keyboard-driven controls, but back then it was just fine.

However, I believe that GNOME is just not designed to properly work as a Wayland compositor.

Basically, if we generalize a little, GNOME Shell is a plugin for Mutter, the actual window manager. This means that
everything in the GNOME Shell tied directly to the window manager. On top of that, the GNOME Shell itself also has
plugins in the form of extensions, which allow you to modify the environment for yourself. While tying the Shell to
the window manager, in my opinion, is sketchy here too, it mostly works out, because there's another layer below the
Shell in case something goes wrong: Xorg. However, when the training wheels come off and you move to Wayland, that
layer of security is no longer there and if something now goes wrong in the window manager, everything collapses.
Basically, if your Wayland window manager/compositor unexpectedly quits, your entire session will go down with it.
And in GNOME, at least in my experience, I got to experience this fact too many times.

The issue here is that GNOME Shell is directly tied into the Wayland compositor, since it's basically just a plugin 
for said compositor. On top of that, the Shell itself has plugins run on top of it. And if any of these pieces on
this stack misbehave, you are going to have problems. I had GNOME extensions that ran so slowly, my entire GNOME
session would freeze for half a second every time it updated. I had the session crash entirely when a plugin failed
to install properly. So on and so forth.

Sway, to me, represents a form of Wayland compositor that is sane: it's small, fast and its features are distributed
among multiple, swappable pieces. If I decide I want to switch from swaybar to waybar, I can do so. I can also throw
both of those options out the window and write my own panel. Or have no panel at all. If the panel gets stuck because
I wrote it poorly, I can just kill the panel and restart it or fire up a different one.

I hope that other Wayland compositors take note of the modular nature of Sway and apply that kind of thinking to their
projects. In an optimal scenario we'd even be able to mix and match components from different desktop environments, so
I could take GNOME's status bar or notification daemon and run it in my Sway session.

## Conclusions

I've really liked Sway and using it as my day-to-day desktop environment has been a kind of an adventure for me.
Going into this I didn't really expect to be staying on Sway afterwards but having seen how well it fulfills my
usecases, I don't think I'll be going back to i3 so soon. Now that I have OBS working on my system, there isn't
anything left that really ties me to Xorg, so I think I'll be extending my stay for now. I'll probably need to
do a bit more hacking to get things like Mako working how I'd like them to, but that's not really a bad thing. After all,
ever since I switched to i3 I have been hacking and tuning things, so hacking my notification daemon is just a natural
extension of that process. Not to mention, wlrobs still has some features I'd like to steal, so there's another reason
to continue hacking.

Anyway, I will still always have i3 just a few button presses away, just in case I'd happen to need it.

If you want to check out how I configured my Sway (and various other pieces of software), you can check out
my dotfiles at [my Gitlab dotfiles repository](https://gitlab.com/Samsai/dotfiles).
