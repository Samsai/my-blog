---
title: "My text editor journey from Visual Basic 2005 to Doom Emacs"
date: 2020-07-17T15:40:13+03:00
tags:
  - tech
  - programming
categories:
draft: false
---

To a programmer, the text editor is among the most important tools. It forms the
interface between the programmer's body and the software being worked on.
However, it's not a tool in the way a hammer is a tool. It's more like a
hand-to-hand weapon, there are many different ones and the only common aspect
you can expect between them is the end goal.

I've used a few text editors over the years and I figured I'd share my journey
from an editor to another and what I picked up or left behind along the way.

## Beginnings

My first experiences with coding were when I was in primary school, probably
around grade 5 or 6. I was toying with the idea of making my own games and
somehow my child brain had through cultural osmosis figured out that coding had
something to do with that. So, I asked my dad about learning to program and got
handed a CD with Visual Basic 2005. That was my first contact with the world of
IDEs and programmer's editors. Obviously back then I had no idea what to even
expect out of these tools and at this point I honestly don't even really
remember what VB2005 looks like.

Eventually I grew bored of drawing basic GUIs and plugging code into button
handlers and found Turbo Pascal 5 on a CD that contained my dad's old DOS stuff.
I also found a paper manual with the description of the software and the
language. Writing stuff in Turbo Pascal seemed more interesting to me because I
was actually writing programs rather than just combining mostly pre-made UI
components.

![Turbo Pascal 5 in DOSBox](/img/editors/turbo-pascal.png)

Jumping from VB2005 back to Turbo Pascal 5 was obviously a big downgrade. I
actually went back to try Turbo Pascal to check some of my old, horrendous code
and nowadays it's pretty awful to use for me. But, that just goes to show that I
had no idea about text editing comfort back in the day. I bet I didn't even know
how to properly use undo/redo or copy-paste.

Basically this period of time was mostly defined by my complete lack of
preference in terms of editors. I didn't know what was out there and I basically
just used whatever landed in front of me. It also didn't even cross my mind that
one editor could be used for writing multiple programming languages. When I
wrote Visual Basic I used the Visual Basic editor, when I wrote Pascal I used
Turbo Pascal.

## Switch to Linux, journey begins

My world expanded pretty world when I switched to Linux in 2010, in terms of
computing, programming and text editors. One of the obvious things was that I
couldn't just use Visual Basic 2005 anymore, so that was out. I also learned
about a language called Python, which I found to be a wonderful language and
it became the focus of my coding and hacking adventures. The editor I ended
up with for writing Python was IDLE, the official development environment that
comes with Python.

![IDLE in action](/img/editors/idle.png)

The choice to use IDLE was once again basically made for me, since it was what
the YouTube tutorials I was watching were using (my main source of tutorials was
a channel called [TheNewBoston](https://www.youtube.com/user/thenewboston/featured))
so that was what I also used.

IDLE served me well, despite its big limitations. Calling it a development
environment isn't really accurate, it's a REPL that allows you to open text
files, one per window. Nowadays it would be totally inadequate for me, but back
when my programs fit into a single file it was all I needed. The REPL was
particularly helpful, since it allowed me to try new things quickly.
Particularly the REPL's auto-completion allowed me to very quickly check what
kinds of functions were available without needing to dive into documentation.

Eventually my programs started to grow in size and I also wanted to explore more
programming languages, which meant that I was starting to run into the limits of
what IDLE could provide. The Minecraft craze of early 2010s drove me to learn
some Java, which meant I spent a bit of time in Eclipse but I wasn't a massive
fan of Java and I found Eclipse quite slow, bloated and even confusing.

Eventually I landed on what I would consider my first "programmer's editor":
[Geany](https://www.geany.org/).

![Geany](/img/editors/geany.png)

Geany calls itself an IDE, but compared to modern IDEs it's definitely a bit of
a light-weight. It can be configured to run and build multi-file projects and it
has support for a wide variety of languages, although that support basically
just boils down to syntax highlighting and tag detection. It's definitely not
advanced but if your needs slightly exceed Gedit but you don't want an entire
IDE, it's a decent middle ground.

Geany became my main editor when I started dabbling with other languages and
multi-file projects. I mainly used it for Python, but I also could rely on it
when I tried a bit of C, Assembly, HTML, Pike, D, C++ etc. It felt good not to
be constantly switching editors for each language I was using and although I
don't think I ever took full advantage of the powers Geany offered, having a
consistent interface to work with regardless of the project I was working on was
a power I could already recognize. In fact, Geany was still my main editor when
I entered university. The only times I used another editor were when I worked
with Java, which as a language is difficult to deal with without proper
IDE-level, context-sensitive auto-completion.

## Vim and Neovim

On the latter half of 2018 I decided to try out a tiling window manager (i3wm)
and fell in love with a keyboard driven approach to window management. This is
when I first ran into a slight problem: I could manage my windows quickly and
easily using my keyboard but when I had to get things done, I would often need
to take my hands off my keyboard and click around with the mouse. I knew the
basic keyboard shortcuts for Geany but I either didn't know or didn't have
shortcuts for many of the more advanced features I required.

Instead of dealing with a perceived reduction in my efficiency, I went looking
for advice from the Unix sages. Following their example, I decided to try out
[Vim](https://www.vim.org/). And then I relatively quickly moved to
[Neovim](https://neovim.io/), because apparently the cool kids were using that.

![Neovim](/img/editors/neovim.png)

I very quickly fell in love with Vim. I had toyed with the vi keybindings in
the past on a basic level (useful skill to have when you Linux systems), so
it didn't take me too long to get over the modality aspect of Vim. Vi keys
just made a lot of sense to me.

However, Vim on its own would have not been enough. I was aware of the rich
ecosystem of plugins surrounding Vim and relatively quickly I had pulled in
enough plugins to cover most of my needs.

However, I think one of the biggest things aspects I grew to like was the fact
that I was in control of how the interface worked. Vim allows you to fully
define what key does what in which mode. This meant that if I didn't like a
particular Vim keybinding, I could replace it with my own. Among the first
changes I made were the tab switching controls (gt and gT), followed by the
window management controls (Ctrl+w h/j/k/l), which I bound to controls that
better mirrored my window manager controls.

Over time I accumulated a fair number of my own custom keybindings. I was also
able to replicate nice workflows and keybindings from other Vim users'
configurations or from completely different editors. I even started trying
niche, vi-like text editors like amp.rs to try and find workflow optimizations I
hadn't yet discovered. One of the biggest revelations to me was opening a fuzzy
file founder with Ctrl+P (an idea which likely originates from Sublime Text).
Using a fuzzy finder lead to me basically discarding tabs entirely and I would
instead just quickly hop from file to another using the file finder.
Experimenting with amp.rs lead me to another optimization, namely using the
spacebar key to run the fuzzy finder instead. Now I could just hit a single key
and entire file trees would be instantly searchable.

This near-infinite customization put me in control of how I wanted to accomplish
essentially any text editing or programming task I could imagine in exactly the
way I wanted. And the modal nature of Vim meant that instead of punching in
various modifier key combos, I could basically talk a kind of language to
express the text transformation I desired. Need to change the contents of
parentheses? Simple, that's just "ci(". Change Inside (). Wanna delete a word?
Just "dw". So, while Vim might initially seem alien, it actually becomes pretty
intuitive when you learn you often deal with just verbs and nouns.

Complete customization and modal editing increased my efficiency by a fair bit
and I felt like I was reaching a kind of text editing nirvana.

## Doom Emacs, the great operating system now with a decent editor

So, I was really happy with Vim (Neovim), why did I move on? It was actually
mostly just by accident. I was working on a software engineering project for a
university course. This was basically a pretty ordinary Java project and since I
hadn't quite figured out a good Java workflow for Vim, I was using Netbeans.
However, I was doing most of the programming on my relatively crappy laptop
which made using Netbeans (or Eclipse) a pretty painful experience. The
combination of boredom and struggling with RAM and CPU limitations made me
experiment with trying to find an alternative Java development environment.

At first I tried messing with Vim, but I couldn't quite get everything working
the way I wanted. Particularly getting auto-completion working with Java seemed
annoying. So, I went looking around and found SpaceVim, which then lead me to
Spacemacs and then [Doom Emacs](https://github.com/hlissner/doom-emacs).

![Doom Emacs](/img/editors/doom-emacs.png)

Doom Emacs seemed intriguing to me. I had very briefly tried Spacemacs but I
found it a bit slow and clunky, not to mention monolithic. Doom Emacs promised
less framework in its configuration framework, along with fast startup times,
opinionated but not overly stubborn defaults and Vim keybindings.

The software project was at a point where I could afford to lose a bit of
momentum, so I figured why not? If the Vim keybindings provided were good enough
I couldn't possibly be much less efficient than I would have been with just Vim.

And Doom Emacs just clicked. I found the Vim keybindings (provided by Evil,
Emacs Vi Layer) to be excellent. Doom also allowed me to enable a number of
modules, which are collections of pre-configured Emacs packages. There was even
a module for Java programming, which was honestly good enough for me to stick
to Doom Emacs for the rest of the software project.

One specialty of SpaceVim, Spacemacs and Doom Emacs is that they are based on a
spacebar-oriented workflow. They use spacebar as a "leader key" and many
features are accessed with key sequences starting with spacebar. It took a while
to figure out what key lead to where, but Doom Emacs comes with which-key, which
interactively tracks what options are available for you based on the keys you've
pressed. As an example ```SPC p p``` opens the project finder (Projectile) and 
```SPC g g``` gets you into a Git interface called Magit.

I got used to using this spacebar-oriented approach so much that I went back and
ported it to my Vim configuration. However, I continued to explore Doom Emacs,
making it my main text editor of choice. One particularly interesting thing I've
found about is that, just like Vim, it's infinitely customizable. With Vim my
configurations never went deeper than setting a few variables and creating
keybindings. I never learned how to write Vimscript to actually program the
editor. But when it comes to Emacs, you configure the entire thing by
programming in Emacs Lisp. And the whole editor is geared towards this. If
there's a command you can run, that's most likely a function written in Elisp.
Emacs also provides functions like describe-key and describe-function to figure
out what function a particular key or key combo is bound to and what that
function does, respectively. You can also evaluate Elisp expressions anywhere,
either in regions of text or in entire buffers. Basically anything could be
rebound or redefined anywhere.

This level of control was initially scary but also intoxicating. It took me a
while to learn how things work in Elisp and I've barely just scratched the
surface. 

Emacs can, or can be made to, do just about anything. It by default comes with
RSS readers, web browsers, email clients, games, file managers and more. And it
has already started consuming my computer, replacing many of my applications
with its variety of modes and packages. At this point I'm already using Emacs to
read my RSS/Atom feeds with Elfeed and I browse Gemini and Gopher content using
Elpher. I also manage my files with Dired, manage git repositories with Magit
and I've even done a bit of chatting on IRC using ERC. Most of my note taking
was moved to the amazing org-mode very quickly after I learned of its existence.
Emacs is so integral to how I use my computer that I launch it as a user-level
systemd service when I log in and I just connect to it using emacsclient.

Emacs makes it possible to use a unified interface for all of these things,
which is similar to the reasons why I switched to Vim in the first place. But
now, in addition to not having to lift my fingers from the keyboard I also have
a centralized location where I can define my keybindings. I can if I want to,
although I haven't really done this in practice yet, combine features from
different packages with a bit of Elisp to do interesting things. I've heard
some people use a snippeting package with an Emacs email client to compose
mail, for example.

## Conclusions

And now we have reached the current stage of my journey. I can't know for
certain if this is where my journey ends, I will most likely at least discover
new and exciting workflows and integrate them into my Emacs configuration. I've
also toyed with the idea of rolling an entirely custom Emacs configuration
without Doom Emacs, but I don't think I would gain anything from it. As it
stands, Doom Emacs is the best text editing experience I've encountered and
it's starting to conquer other aspects of computing as well. It would take
a fair amount of effort to convince me to go elsewhere at this point.

But to quickly recap what I learned along the way:
- Visual Basic and Turbo Pascal taught me to love coding.
- IDLE taught me the usefulness of a REPL and exploratory programming.
- Geany taught me the convenience of using a single editor for many languages.
- Vim taught me the efficiency of modal editing and completely customized key bindings.
- (Doom) Emacs taught me the value of a fully programmable coding environment.

It may also be worth pointing out that although I present my journey through
just these editors I've highlighted here, I have tried various other editors,
but these visits have generally been brief. Particularly things like Atom I
could just about tolerate only for an afternoon and VS Code I haven't tried
at all. The editors I've mentioned here are ones I've spent a decent amount
of time with and I consider important milestones that have taught me
something.

There are also editors I've spent time with, but haven't included except
as quick remarks, such as Eclipse and Netbeans. The reason for this is
that I don't particularly like them and I only reluctantly use them to
solve the problem of editing complex Java codebases. If/when I can use
other tools for this, I will, and I generally consider having to rely
on IDE behemoths to be more of a fault of Java rather than a deficiency
in other editors.
