---
title: "Thoughts on my attempt at coding streams"
date: 2019-07-31T21:53:30+03:00
tags:
  - programming
categories:
draft: false
---

I've recently tried a new and scary experiment of coding on a livestream. I have been streaming
for a good while now (would be multiple years by now) but I've only really played video games on
my streams and that's a relatively simple process, since the game being played provides the
framework to build your own content and entertainment on. Sometimes I have streamed some games
that have involved a degree of programming, such as TIS-100 (highly recommended) but I haven't
really done anything that would require further creativity on my part.

However, as of late my interests have shifted a little bit from just playing video games towards
my other hobby, my point of study and my probable future career path, which is coding. I've been
coding as a hobby for the better part of a decade now, I'm studying computer science at a respected
university and I have a few public projects under my belt that I am quite proud of. So, I've sometimes 
taken the opportunity to showcase some of the things that I have been writing on a few streams, but 
I hadn't actually done any of the coding on a stream until a couple weeks ago.

I still struggle a fair bit with impostor syndrome and I often feel uncertain when it comes to
showing my talents or advertising myself. I honestly sometimes feel bad about announcing livestreams
on social media because I feel like I am taking up other people's space on their virtual timelines.
So, I was a bit anxious about doing what is arguably a creative activity online and in front of other
people and was worried that the code I would write would be totally idiotic and wrong.

But I do have to say that the few coding streams I've done have gone rather well. For these coding 
streams I decided to work on my [Rust Roguelike](https://gitlab.com/Samsai/rust-roguelike)
which is a rather simple roguelike I've been making using the Rust programming language and SDL2.
I've got some rather encouraging feedback that has allowed me to become more comfortable with
showing my code to other people and realize that maybe I have been overly dismissive of my own
abilities and skills. 

These streams have also been surprisingly productive. I expected a massive hit to my productivity due 
to speaking at the same time as typing things out and having to go on long debugging adventures
because of a loss of focus, but I have been able to churn out reasonable code at a fairly good rate.
Because of the stream, I think I have actually been more focused, since I cannot just fire up YouTube
or Netflix and just wander away from my Vim instance. Not to mention the helpful feature suggestions
on the part of the chat, which is just great for a roguelike, where features are plenty and the sky's
the limit.

The only problem I've had with these streams is my lack of planning for the streams thus far. I 
started doing them quite ad-hoc so I haven't really laid out any plans on what to work on for each 
stream, so we've mainly been doing quick new features that we've come up with on the spot. This 
has sometimes lead to some pauses, where I've had to come up with something to do when moving from 
one task to another. So, this is one issue I do hope to improve on.

Overall, I think doing these coding streams have been a fun and helpful exercise and I think they are
worth doing in the future. I'm not entirely sure how many of my typical viewers are interested in such
streams, but I do know that a few of them are coder types as well, so I hope at least someone is interested
in them. So, for the foreseeable future I will keep doing them on Sundays when I feel like I have the
time, energy and a project to stream. I am hoping to have Rust Roguelike wrapped up in some kind of 
a 1.0 state soon so that I can maybe move on to another new game project at some point.

I am also planning a blog post about the Rust Roguelike project at some point, so you'll probably
be hearing more about it and my journey of writing it at some point when I consider the design
to be complete enough.
