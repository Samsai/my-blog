---
title: "Domain change from samsai.online to samsai.eu"
date: 2019-07-25T10:25:35+03:00
tags:
  - meta
categories:
---
I recently went and checked my domain expiration date and its renewal price and have come to the conclusion that
it would be cheaper for me to change my domain. I registered samsai.online because it was rather cheap for the
first year but didn't realize that the renewal price would be on the slightly higher side. So, I decided to register 
a new domain which has a slightly cheaper renewal rate.

This means that this website will be accessible at [samsai.eu](https://samsai.eu) from now on and the old
samsai.online domain will expire in October of this year. This means that if you have this website bookmarked
or you are subscribed to the RSS feed, you have until October to change the addresses in the relevant places.

That is all for now.
