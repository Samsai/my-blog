---
title: "Streaming"
categories:
draft: false
---

# Livestreams

I stream on [Owncast](https://cast.samsai.eu) three times a week, playing exclusively Linux games.
My aim is to produce enjoyable, but informative content about Linux gaming through a live, first
impressions format.

Streams are timed so that they start at around 8 PM Finnish time, which means 5 PM UTC in the summer time
and 6 PM UTC in the winter time.

## Past livestreams

Sometimes streams may consist of multiple videos due to crashes. Sometimes I will merge them together prior to uploading, sometimes not.

VOD archive has moved again: https://vods.samsai.eu

## Friday Livestream

On Fridays we play a selection of titles that are relevant to current events in Linux gaming. Typically
these include things new releases and games that have been sent to me either by developers or by
viewers. The aim is to run the show as a sort of weekly changing picture into Linux gaming and
offer some semi-critical first impressions on new games that are coming out.

The latter half of the Friday Livestream is typically reserved for a longer term playthrough of a
Linux game. The games are picked by me with recommendations from the community. The games played
are typically slightly older to give people time to play them on their own so as to avoid spoiling
games to others.

Currently playing through: TBD

## Casual Saturday Streams

As the name implies, Casual Saturdays are slightly less structured and usually consists of me playing
a game I personally feel like playing. Usually this means games that I can easily get into and put
down, for example various roguelike/roguelite games like Caves of Qud and Slay the Spire, or multiplayer
games.

Sometimes we also start extra playthroughs on Saturdays on games that I really want to play through
on stream but the Friday slot is already taken.

The Casual Saturdays are also often less focused on the particular game being played and offer a place
for me and the chat to talk about all sorts of things. So, it's the perfect stream to just drop into
and chat, if you feel like doing so.

## Casual Sunday Coding

A relatively new experiment which has turned into a regular broadcast. On Sunday's I will work on
some programming projects of mine, usually related to game programming and game development, although
not fundamentally tied to just that subject. Originally I started the coding streams to get over
some of my own impostor syndrome and to also get me to work on programming weekly by having the
stream hold me accountable. On both of these counts the streams have been quite successful.

The intention is to share some of the joy, success and failure I experience with programming with my 
viewers and also slightly involve them with whatever I am creating in the form of feedback and
brainstorming.

It's not supposed to be a tutorial, nor do I particularly invite overt criticism of my own skills,
but instead I'll work on something and try to explain my process as I do so and discuss it with
the chat. Other programming, game dev and Linux related conversation usually also takes place and
is definitely welcome.

# The Livestream Memeosphere

Here's a list of the various inside-jokes and memes we have accumulated over the many livestreams.
The list will be updated as the need arises.

### Infinite Shatters

The game World of Castles (if it can be called a game) had an interesting spelling for the word
"shader". Infinite Shatters usually refers to graphical glitches or to describe a graphically
complex game.

### Pre-recorded

Sometimes a death or a level change happens on almost exactly at an hour mark when a game is
supposed to change. Often this leads to conspiracy theories of the stream being pre-recorded.
I assure you, however, that they aren't. Honest.

### Only the Fish can Roll

Unsurprisingly arose during a Nuclear Throne stream. Usually evoked when someone does a combat
roll or fish are involved in some way.


