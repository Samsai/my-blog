all: hugo pagefind sync

hugo:
	hugo

pagefind:
	npx pagefind --source public --bundle-dir _pagefind 

sync: hugo pagefind
	rsync -r -a -v -e ssh --delete-before --progress public/ root@samsai.eu:/var/www/html
